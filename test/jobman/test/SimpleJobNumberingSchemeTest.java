/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jobman.test;

import jobman.database.jobnumberingschemes.SimpleJobNumberingScheme;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author worralr
 */
public class SimpleJobNumberingSchemeTest {
    
    private SimpleJobNumberingScheme instance;
    
    @Before
    public void setup(){
    instance = new SimpleJobNumberingScheme();
    
    } 
    
        @Test
   public void shouldIncrementJobNumber(){
   assertThat(instance.issueJobMainNumber(0), equalTo("1"));
   assertThat(instance.issueJobMainNumber(1), equalTo("2"));
   assertThat(instance.issueJobMainNumber(2), equalTo("3"));
   assertThat(instance.issueJobMainNumber(3), equalTo("4"));
   }

    @Test
    public void shouldIncrementSubJobNumber() {
         assertThat(instance.issueSubJobNumber("1", 0), equalTo("1-1"));
         assertThat(instance.issueSubJobNumber("1", 1), equalTo("1-2"));
         assertThat(instance.issueSubJobNumber("1", 2), equalTo("1-3"));
         assertThat(instance.issueSubJobNumber("1", 3), equalTo("1-4"));
    }
}

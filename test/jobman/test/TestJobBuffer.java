/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jobman.test;

import java.util.ArrayList;
import JobTypes.MainJob;
import jobman.database.JobBuffer;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;



/**
 *
 * @author worralr
 */
public class TestJobBuffer {
    
    JobBuffer instance;    
    
    @Before
    public void setup(){
     instance = new JobBuffer();
    }

@Test    
public void shouldBufferJobAndReturnTrueWhenAskedIfBuffered(){
    MainJob mainJob1 = new MainJob("1");    
    instance.bufferJob(mainJob1);
    assertTrue(instance.jobBuffered("1"));
}

@Test
public void shouldReturnWhenWhenAskedIfJobBufferedWhenItIsnt(){
    assertTrue( ! instance.jobBuffered("fgdfgdfsgdffds"));
}   
 
@Test
public void shouldBufferJobAndReturnJobWhenAsked(){
    MainJob mainJob2 = new MainJob("2"); 
    instance.bufferJob(mainJob2);
        MainJob retreivedJob = instance.getJob("2");
       
    assertThat(retreivedJob, equalTo(mainJob2));
}

@Test
public void shouldReturnNullWhenJobNotBuffered(){
        MainJob retreivedJob = instance.getJob("gdfdrjglkdrsjdsl");
    assertThat(retreivedJob, equalTo(null));
}

@Test
public void shouldRemoveJob(){
 MainJob mainJob3 = new MainJob("3");    
 instance.bufferJob(mainJob3);
 instance.removeJob("3");
    MainJob retreivedJob = instance.getJob("3");
    assertThat(retreivedJob, equalTo(null));
}

    @Test
    public void shouldEmptyRemoveOldestItemInBufferWhenAddingJobOverTheCapacity() throws Exception {
        //create a new instance as need ensure it starts empty
        JobBuffer anotherInstance = new JobBuffer();
        //create an array of a hundred and one jobs
        ArrayList<MainJob> jobList = new ArrayList<>();
        for (int i =0; i<=100; i++){    
        jobList.add(new MainJob(""+i));
            
        anotherInstance.bufferJob(jobList.get(i));
        //introduce a space between the timing of instances added to the buffer. 
        try{
            Thread.sleep(1);
        }
        catch (InterruptedException e){
        }
        }
       
          
          MainJob retrievedJob1 = anotherInstance.getJob("0");
          MainJob retrievedJob2 = anotherInstance.getJob("1");
          assertThat(retrievedJob1, equalTo(null));
          assertThat(retrievedJob2, equalTo(jobList.get(1)));
         
    }

}

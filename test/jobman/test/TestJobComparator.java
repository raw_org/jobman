/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jobman.test;

import JobTypes.MainJob;
import jobman.database.jobnumberingschemes.JobComparatorByJobNumber;
import static org.hamcrest.CoreMatchers.equalTo;

import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;


/**
 *
 * @author worralr
 */
public class TestJobComparator {
    
    JobComparatorByJobNumber instance ;
    
    @Before
    public void setup(){
    instance = new JobComparatorByJobNumber();
    }
    
    @Test
    public void shouldCompareTwoMainJobsAsEqual() {
        MainJob instance1 = new MainJob("1");
         MainJob instance2 = new MainJob("1");
         assertThat(instance.compare(instance1, instance2), equalTo(0));
    }

    @Test
    public void shouldCompareTwoMainPuttingThemNumericalOrderByJobNumber() {
        MainJob instance1 = new MainJob("1");
         MainJob instance2 = new MainJob("2");
         assertThat(instance.compare(instance1, instance2), equalTo(1));
    }
        @Test
    public void shouldCompareTwoMainPuttingThemNumericalOrderByJobNumber2() {
        MainJob instance1 = new MainJob("1");
         MainJob instance2 = new MainJob("2");
         assertThat(instance.compare(instance2, instance1), equalTo(-1));
    }
    
          @Test
    public void shouldCompareTwoMainPuttingThemNumericalOrderByJobNumber3() {
        MainJob instance1 = new MainJob("1-2");
         MainJob instance2 = new MainJob("1-3");
         assertThat(instance.compare(instance1, instance2), equalTo(1));
    }
    
            @Test
    public void shouldCompareTwoMainPuttingThemNumericalOrderByJobNumber4() {
        MainJob instance1 = new MainJob("1-2");
         MainJob instance2 = new MainJob("1-3");
         assertThat(instance.compare(instance2, instance1), equalTo(-1));
    }
    
           @Test
    public void shouldCompareTwoMainPuttingThemNumericalOrderByJobNumber5() {
        MainJob instance1 = new MainJob("1-2-2");
         MainJob instance2 = new MainJob("1-3-2");
         assertThat(instance.compare(instance1, instance2), equalTo(1));
    }
}

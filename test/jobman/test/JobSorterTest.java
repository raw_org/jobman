/*
 Copyright (c) <2008>, <Robert Worrall>
 *  All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
    * Neither the name of the <ORGANIZATION> nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package jobman.test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import jobman.JobSorter;
import JobTypes.MainJob;
import jobman.database.JobDataBaseDB4OEmbedded;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.when;
import org.mockito.MockitoAnnotations;



/**
 *
 * @author Robert Worrall
 */
public class JobSorterTest {
    
    @Mock 
    private JobDataBaseDB4OEmbedded jobDataBase;
        
    @InjectMocks
    JobSorter sorter;
    
    final MainJob job1 = new MainJob();
    final MainJob job2 = new MainJob();
    final MainJob job3 = new MainJob();
    final MainJob job4 = new MainJob();
    final MainJob job5 = new MainJob();
    final MainJob job6 = new MainJob();
     
    final Date today = new Date();
    final long dayInMilliseconds = 86400000;
    final Date oneDayBeforeToday = new Date(today.getTime()-dayInMilliseconds);
    final Date twoDaysBeforeToday = new Date(today.getTime() - (dayInMilliseconds * 2));
    final Date tomorrow = new Date(today.getTime() + dayInMilliseconds);
    final Date next59Minutes = new Date(today.getTime() + (3599));
    
    final ArrayList <MainJob> testList = new ArrayList(
    Arrays.asList(
    job3,job1,job2,job4,job5
    ) );
    
    
            
    
    
    @Before
    public void setup(){
       job1.setReminder(oneDayBeforeToday);
       job1.setName("Job1");
       job2.setReminder(twoDaysBeforeToday);
       job2.setName("Job2");
       job3.setReminder(today);
       job3.setName("Job3");
       job4.setReminder(tomorrow);
       job4.setName("Job4");
       job5.setReminder(next59Minutes);
       job5.setName("Job5");
       
       job6.setReminder(today);
       job6.setName("Job6");
            
               
    sorter = new JobSorter();
        MockitoAnnotations.initMocks(this);
    }
    
    @Test
    public void shouldSortJobsDueNow() {
        ArrayList<MainJob> actual = sorter.sortJobsDueNow(testList);
       //ArrayList<MainJob> expectedList = new ArrayList<>(Arrays.asList(job3,job1,job2));
        assertThat(actual.size(), equalTo(3));
        assertTrue(actual.contains(job1));
        assertTrue(actual.contains(job2));
        assertTrue(actual.contains(job3));
        
    }

    @Test
    public void shouldReturnJobsDueInTheNextHour() {
        ArrayList actualList = sorter.sortJobsDueInTheNextHour(new HashSet(testList));
        assertThat(actualList.size(), equalTo(4));
        assertTrue(actualList.contains(job1));
        assertTrue(actualList.contains(job2));
        assertTrue(actualList.contains(job3));
        assertTrue(actualList.contains(job5));  
    }
    
    @Test
    public void shouldReturnSubJobsDueAsWellAsMainJobs()throws Exception{
    job5.setJobNumber("5");
    job6.setJobNumber("5-6");
    job5.addSubJob("5-6");
    when(jobDataBase.findJobByJobNumber("5-6")).thenReturn(job6);
    ArrayList actualList = sorter.sortJobsDueInTheNextHour(new HashSet(testList));
        assertThat(actualList.size(), equalTo(5));
        assertTrue(actualList.contains(job1));
        assertTrue(actualList.contains(job2));
        assertTrue(actualList.contains(job3));
        assertTrue(actualList.contains(job5));
        assertTrue(actualList.contains(job6));
        
    }
    
}

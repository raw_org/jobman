/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jobman.test;

import JobTypes.BaseTypes.Job;
import com.db4o.ext.DatabaseClosedException;
import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.Set;
import JobTypes.MainJob;
import JobTypes.MainJobReplicator;
import JobTypes.BaseTypes.SpawnableJob;
import jobman.database.JobDataBaseDB4OEmbedded;
import static org.hamcrest.CoreMatchers.equalTo;
import org.junit.After;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author worralr
 */
public class JobDataBaseTest {

    JobDataBaseDB4OEmbedded instance;
    public static final String databaseName = "testing.jmdb";
    public static final String jobNumberingScheme = "jobman.database.jobnumberingschemes.SimpleJobNumberingScheme";

    public JobDataBaseTest() {
    }

    @Before
    public void setup() throws Exception {
        instance = new JobDataBaseDB4OEmbedded();
        instance.createDatabase(databaseName, jobNumberingScheme);
    }

    @After
    public void afterTests() {
        new File(databaseName).delete();

    }

    public void emptyDatabase() {
        Set<MainJob> allCurrentMainJobs = instance.getAllCurrentMainJobs();
        for (MainJob aJob : allCurrentMainJobs) {
            instance.deleteJob(aJob);
        }
        Set<Job> allFinshedMainJobs = instance.getAllFinshedMainJobs();
        for (Job aJob : allFinshedMainJobs) {
            instance.deleteJob(aJob);
        }
        Set<SpawnableJob> replicatableJobs = instance.getReplicatableJobs();
        for (SpawnableJob aJob : replicatableJobs) {
            instance.deleteReplicableJob(aJob);
        }
    }

    @Test
    public void shouldCreateDataBaseSoFileShouldExist() throws Exception{
        instance.createDatabase("testingCreationDataBase.jmdb", jobNumberingScheme);
        File newDatabase = new File("testingCreationDataBase.jmdb");
        assertThat(true, equalTo(newDatabase.exists()));
        newDatabase.delete();

    }

    @Test
    public void should_create_database_and_keep_number_order_after_closing() throws Exception {
        instance.createDatabase("testingCreationDataBase.jmdb", jobNumberingScheme);
        instance.loadDatabase("testingCreationDataBase.jmdb");
        String aJobNumber = instance.issueJobNumber();
        instance.closeDatabase();
        instance.loadDatabase("testingCreationDataBase.jmdb");
        String anotherJobNumber = instance.issueJobNumber();
        instance.closeDatabase();
        assertThat(aJobNumber, equalTo("1"));
        assertThat(anotherJobNumber, equalTo("2"));
        File newDatabase = new File("testingCreationDataBase.jmdb");
        newDatabase.delete();
    }

    @Test
    public void shouldAddAndRecallAndThenDeleteMainJob() {
        instance.loadDatabase(databaseName);
        MainJob aJob = new MainJob();
        aJob.setJobNumber(instance.issueJobNumber());
        instance.storeJob(aJob);
        MainJob retreviedJob = instance.findJobByJobNumber(aJob.getJobNumber());
        instance.closeDatabase();
        assertThat(aJob, equalTo(retreviedJob));

    }

    @Test
    public void ShouldDeleteJob() {
        instance.loadDatabase(databaseName);
        MainJob aJob = new MainJob();
        aJob.setJobNumber(instance.issueJobNumber());
        instance.storeJob(aJob);
        instance.deleteJob(aJob);
        MainJob retreviedJob = instance.findJobByJobNumber(aJob.getJobNumber());
        instance.closeDatabase();
        assertThat(null, equalTo(retreviedJob));

    }

    @Test
    public void shouldReturnEmptyMainJobList() {
        instance.loadDatabase(databaseName);
        emptyDatabase();
        Set<MainJob> allCurrentMainJobs = instance.getAllCurrentMainJobs();
        instance.closeDatabase();
        assertTrue(allCurrentMainJobs.isEmpty());

    }

    @Test
    public void shouldReturnAllMainJobsButNotSubJobs() {
        instance.loadDatabase(databaseName);
        emptyDatabase();
        MainJob aJob = new MainJob();
        aJob.setJobNumber(instance.issueJobNumber());
        instance.storeJob(aJob);
        MainJob aJob2 = new MainJob();
        aJob2.setJobNumber(instance.issueJobNumber());
        MainJob aJob3 = new MainJob();
        aJob3.setJobNumber(instance.issueSubJobNumber(aJob2.getJobNumber(),aJob2.getLinkedJobs().size()));
        instance.storeJob(aJob2);
        instance.storeJob(aJob3);
        Set<MainJob> allCurrentMainJobs = instance.getAllCurrentMainJobs();
        boolean aJobRetreived = allCurrentMainJobs.contains(aJob);
        boolean aJob2Retreived = allCurrentMainJobs.contains(aJob2);
        boolean aJob3Retreived = allCurrentMainJobs.contains(aJob3);
        instance.closeDatabase();
        assertTrue(aJob2Retreived && aJobRetreived & !aJob3Retreived);
    }

    @Test
    public void shouldUpdateAJob() {
        instance.loadDatabase(databaseName);
        MainJob aJob = new MainJob();
        aJob.setJobNumber(instance.issueJobNumber());
        instance.storeJob(aJob);
        aJob.setName("Micks shopping list");
        instance.storeJob(aJob);
        MainJob retrevedJob = instance.findJobByJobNumber(aJob.getJobNumber());
        String jobName = retrevedJob.getName();
        instance.closeDatabase();
        assertThat("Micks shopping list", equalTo(jobName));

    }

    @Test
    public void shouldReturnIncompleteJobs() {
        instance.loadDatabase(databaseName);
        emptyDatabase();
        MainJob aJob = new MainJob();
        aJob.setJobNumber(instance.issueJobNumber());

        MainJob aJob2 = new MainJob();
        aJob2.setPercentComplete(100);
        aJob2.setJobNumber(instance.issueJobNumber());
        MainJob aJob3 = new MainJob();
        aJob3.setJobNumber(instance.issueJobNumber());
        instance.storeJob(aJob);
        instance.storeJob(aJob2);
        instance.storeJob(aJob3);
        Set<MainJob> allMainJobs = instance.getAllCurrentMainJobs();

        boolean aJobShouldBeInResults = allMainJobs.contains(aJob);

        boolean aJob2ShouldNotBeInResults = !allMainJobs.contains(aJob2);

        boolean aJob3ShouldBeInResults = allMainJobs.contains(aJob3);

        instance.closeDatabase();
        assertTrue(aJob2ShouldNotBeInResults && aJobShouldBeInResults && aJob3ShouldBeInResults);
    }

    @Test
    public void shouldReturnCompleteJobs() throws Exception {
        instance.loadDatabase(databaseName);
        emptyDatabase();
        MainJob aJob = new MainJob();
        aJob.setJobNumber(instance.issueJobNumber());
        instance.storeJob(aJob);
        MainJob aJob2 = new MainJob();
        aJob2.setPercentComplete(100);
        aJob2.setJobNumber(instance.issueJobNumber());
        MainJob aJob3 = new MainJob();
        aJob3.setJobNumber(instance.issueJobNumber());
        MainJob aSubJob = new MainJob();
        aSubJob.setJobNumber(instance.issueSubJobNumber(aJob2.getJobNumber(),aJob2.getLinkedJobs().size()));
        aJob2.addSubJob(aSubJob.getJobNumber());
        MainJob aSubJobComplete = new MainJob();
        aSubJobComplete.setJobNumber(instance.issueSubJobNumber(aJob2.getJobNumber(),aJob2.getLinkedJobs().size()));
        aSubJobComplete.setPercentComplete(100);
        aJob2.addSubJob(aSubJobComplete.getJobNumber());
        instance.storeJob(aJob2);
        instance.storeJob(aJob3);
        instance.storeJob(aSubJob);
        instance.storeJob(aSubJobComplete);
        Set<Job> allFinshedMainJobs = instance.getAllFinshedMainJobs();
        boolean aJobShouldNotBeInResults = !allFinshedMainJobs.contains(aJob);
        boolean aJob2ShouldBeInResults = allFinshedMainJobs.contains(aJob2);
        boolean aJob3ShouldNotBeInResults = !allFinshedMainJobs.contains(aJob3);
        boolean aSubJobShouldNotBeInResults = !allFinshedMainJobs.contains(aSubJob);
        boolean aSubJobCompleteShouldNotBeInResults = !allFinshedMainJobs.contains(aSubJobComplete);
        instance.closeDatabase();
        assertTrue(aJob2ShouldBeInResults && aJobShouldNotBeInResults && aJob3ShouldNotBeInResults && aSubJobShouldNotBeInResults && aSubJobCompleteShouldNotBeInResults);
    }

    @Test
    public void shouldReturnEmptyCatagoryList() {
        instance.loadDatabase(databaseName);
        ArrayList<String> catagoryList = instance.getCatagoryList();
        instance.closeDatabase();
        assertTrue(catagoryList.isEmpty());
    }

    @Test
    public void shouldReturnEmptyReplicatableJobList() {
        instance.loadDatabase(databaseName);
        emptyDatabase();
        Set<SpawnableJob> replicatableJobs = instance.getReplicatableJobs();
        assertTrue(replicatableJobs.isEmpty());
        instance.closeDatabase();
    }

    @Test
    public void shouldReturnAReplicatableJob() {
        instance.loadDatabase(databaseName);
        emptyDatabase();
        MainJob aJob = new MainJob(instance.issueJobNumber());
        MainJobReplicator repjob = new MainJobReplicator();
        repjob.setDateToSpawn(new Date());
        repjob.setExistingJobNumber(aJob.getJobNumber());
        repjob.setJobToBeReplicated(aJob);
        instance.storeReplicatableJob(repjob);
        Set<SpawnableJob> replicatableJobs = instance.getReplicatableJobs();
        boolean containsRepJob = replicatableJobs.contains(repjob);
        instance.closeDatabase();
        assertTrue(containsRepJob);
    }

    @Test
    public void shouldDeleteAReplicatableJob() {
        instance.loadDatabase(databaseName);
        emptyDatabase();
        MainJob aJob = new MainJob(instance.issueJobNumber());
        MainJobReplicator repjob = new MainJobReplicator();
        repjob.setDateToSpawn(new Date());
        repjob.setExistingJobNumber(aJob.getJobNumber());
        instance.storeReplicatableJob(repjob);
        instance.deleteReplicableJob(repjob);
        Set<SpawnableJob> replicatableJobs = instance.getReplicatableJobs();
        boolean doesNotContainsRepJob = !replicatableJobs.contains(repjob);
        instance.closeDatabase();
        assertTrue(doesNotContainsRepJob);
    }

    @Test
    public void shouldFindJobBySearchStringInJobName() {
        instance.loadDatabase(databaseName);
        emptyDatabase();
        MainJob aJob = new MainJob(instance.issueJobNumber());
        aJob.setName("Micks Jaggers Shopping List");
        instance.storeJob(aJob);
        Set<Job> findJob = instance.findJob("Mick");
        boolean containsJob = findJob.contains(aJob);
        instance.closeDatabase();
        assertTrue(containsJob);
    }

    @Test
    public void shouldFindJobBySearchStringInJobNotes() {
        instance.loadDatabase(databaseName);
        emptyDatabase();
        MainJob aJob = new MainJob(instance.issueJobNumber());
        aJob.setName("Micks Jaggers Shopping List");
        aJob.setNotes("1. Shreddies for Jack");
        instance.storeJob(aJob);
        Set<Job> findJob = instance.findJob("Shreddies");
        boolean containsJob = findJob.contains(aJob);
        instance.closeDatabase();
        assertTrue(containsJob);
    }

    @Test
    public void shouldFindJobBySearchStringInOtherCustomerDetails() {
        instance.loadDatabase(databaseName);
        emptyDatabase();
        MainJob aJob = new MainJob(instance.issueJobNumber());
        aJob.setOtherDetails("Get a water pistol for Joe");
        instance.storeJob(aJob);
        Set<Job> findJob = instance.findJob("water");
        boolean containsJob = findJob.contains(aJob);
        instance.closeDatabase();
        assertTrue(containsJob);
    }

    @Test
    public void shouldNotFindJobBySearchStringInOtherCustomerDetails() {
        instance.loadDatabase(databaseName);
        emptyDatabase();
        MainJob aJob = new MainJob(instance.issueJobNumber());
        aJob.setOtherDetails("Get a water pistol for Joe");
        instance.storeJob(aJob);
        Set<Job> findJob = instance.findJob("eggs");
        boolean doesNotContainsJob = !findJob.contains(aJob);
        instance.closeDatabase();
        assertTrue(doesNotContainsJob);
    }

    @Test
    public void shouldFindMainJobByJobNumber() {
        instance.loadDatabase(databaseName);
        emptyDatabase();
        MainJob aJob = new MainJob(instance.issueJobNumber());
        aJob.setOtherDetails("Get a water pistol for Joe");
        instance.storeJob(aJob);
        String jobNumber = aJob.getJobNumber();
        MainJob findJobByJobNumber = (MainJob) instance.findJobByJobNumber(jobNumber);
        boolean jobRetrieved = findJobByJobNumber.equals(aJob);
        instance.closeDatabase();
        assertTrue(jobRetrieved);
    }

    @Test
    public void shouldFilterOutReplicatableJobsWhenGettingAllActiveMainjobs() {
        instance.loadDatabase(databaseName);
        emptyDatabase();
        MainJob aJob = new MainJob();
        aJob.setJobNumber(instance.issueJobNumber());
        instance.storeJob(aJob);
        MainJobReplicator replicatableJob = new MainJobReplicator();
        // replicatableJob.
        MainJob aJob3 = new MainJob(instance.issueJobNumber());
        //aJob3.setJobNumber(replicatableJob.issueSubJobNumber());
        instance.storeReplicatableJob(replicatableJob);
        
        instance.storeJob(aJob3);
        Set<MainJob> allCurrentMainJobs = instance.getAllCurrentMainJobs();
        boolean aReplicatableJobShouldNotBeRetreived = !allCurrentMainJobs.contains(replicatableJob);
        instance.closeDatabase();
        assertTrue(aReplicatableJobShouldNotBeRetreived);
    }

    @Test
    public void closeDatabaseShouldDoNothingWhenNoDatabaseHasBeenOpened() {
        JobDataBaseDB4OEmbedded anotherInstance = new JobDataBaseDB4OEmbedded();
        anotherInstance.closeDatabase();

    }

    @Test(expected = DatabaseClosedException.class)
    public void shouldThrowExceptionwhenSearchingForJobAndNoDatabaseOpen() {
        Set<MainJob> allCurrentMainJobs = instance.getAllCurrentMainJobs();
        assertThat(allCurrentMainJobs, equalTo(null));
    }

    @Test
    public void shouldreturnClassFromBufferWhenSearchingForJobByJobNumberTwice() {
        instance.loadDatabase(databaseName);
        emptyDatabase();
        MainJob aJob = new MainJob();
        aJob.setJobNumber(instance.issueJobNumber());
        MainJob aJob2 = new MainJob();
        aJob2.setPercentComplete(100);
        aJob2.setJobNumber(instance.issueJobNumber());
        MainJob aJob3 = new MainJob();
        aJob3.setJobNumber(instance.issueJobNumber());
        instance.storeJob(aJob);
        instance.storeJob(aJob2);
        instance.storeJob(aJob3);
        MainJob findMainJobByJobNumber = instance.findJobByJobNumber(aJob.getJobNumber());
        MainJob findMainJobByJobNumber1 = instance.findJobByJobNumber(aJob.getJobNumber());

        instance.closeDatabase();
        assertThat(findMainJobByJobNumber, equalTo(findMainJobByJobNumber1));
    }
}

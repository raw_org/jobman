/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jobman.test;

import JobTypes.BaseTypes.Job;
import JobTypes.MainJob;
import JobTypes.MainJobReplicator;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertThat;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author worralr
 */
public class TestMainJobReplicator {

    MainJobReplicator instance;
    MainJob aJobToBeReplicated;
    
    @Before
    public void setup(){
    instance = new MainJobReplicator();
    aJobToBeReplicated = new MainJob();
    aJobToBeReplicated.setName("test replicator");
    aJobToBeReplicated.setCategory("Music");
    aJobToBeReplicated.setOtherReferenceNumber(100);
    aJobToBeReplicated.setNotes("JobNotes");
    aJobToBeReplicated.setJobNumber("1");
    aJobToBeReplicated.setOtherDetails("other details");
    aJobToBeReplicated.setPercentComplete(100);
    
    
    }
    
    @Test 
    public void shouldGetLastSpawnedJobThatMatchesInstance() throws Exception{
        instance.setJobToBeReplicated(aJobToBeReplicated);
        Job spawnedJob = instance.getLastSpawnedJob();              
        assertThat(aJobToBeReplicated.getName(), equalTo(spawnedJob.getName()));
        assertThat(aJobToBeReplicated.getCatagory(), equalTo(spawnedJob.getCatagory()));
        assertThat(aJobToBeReplicated.getOtherReferenceNumber(), equalTo(spawnedJob.getOtherReferenceNumber()));
        assertThat(aJobToBeReplicated.getNotes(), equalTo(spawnedJob.getNotes()));
        assertThat(aJobToBeReplicated.getJobNumber(), not(spawnedJob.getJobNumber()));
        assertThat(aJobToBeReplicated.getOtherDetails(), equalTo(spawnedJob.getOtherDetails()));
        assertThat(spawnedJob.getPercentComplete(), equalTo(0));
        
    }   
   
    @Test
    public void shouldReturnSameInstanceOfMainJobWhenSpawnIsNotCalled() {
        instance.setJobToBeReplicated(aJobToBeReplicated);
        Job spawnedJob = instance.getLastSpawnedJob();  
        Job spawnedJob2 = instance.getLastSpawnedJob(); 
        assertThat(spawnedJob, equalTo(spawnedJob2));
    }
    
    @Test
    public void shouldReturnAnotherInstanceOfMainJobAfterSpawnIsCalled() throws Exception {
        instance.setJobToBeReplicated(aJobToBeReplicated);
        Job spawnedJob = instance.getLastSpawnedJob();  
        instance.spawn();
        Job spawnedJob2 = instance.getLastSpawnedJob(); 
        assertThat(spawnedJob, not(spawnedJob2));
    }
    
    ///// Todo: No working code for spawning subjobs there is an issue with jobs more than 2 instances deep. 
    
    
    
}

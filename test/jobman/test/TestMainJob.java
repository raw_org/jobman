/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jobman.test;

import java.util.ArrayList;
import java.util.Arrays;
import jobman.JobException;
import JobTypes.MainJob;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author worralr
 */
public class TestMainJob {

    private MainJob instance;
    
    @Before
    public void setup(){
    instance = new MainJob();
    }

    @Test
    public void shouldIncrementTime(){
    assertThat(instance.getTimeSpent(), is(0L));
    instance.incrementTimeSpent(100);
    assertThat(instance.getTimeSpent(), equalTo(100L));
    instance.incrementTimeSpent(100);
    assertThat(instance.getTimeSpent(), equalTo(200L));
    instance.incrementTimeSpent(800);
    assertThat(instance.getTimeSpent(), equalTo(1000L));
    }
    
    @Test (expected = JobException.class)
    public void shouldThrowExceptionWhenAddingIncorrectSubJob()throws Exception{
      instance.setJobNumber("1");
      MainJob subjob = new MainJob();
      subjob.setJobNumber("2");
      instance.addSubJob(subjob.getJobNumber());  
}
        @Test
    public void shouldNotThrowExceptionWhenAddingCorrectSubJob()throws Exception{
      instance.setJobNumber("1");
      MainJob subjob = new MainJob();
      subjob.setJobNumber("1-2");
      instance.addSubJob(subjob.getJobNumber());  
      assertTrue(instance.getLinkedJobs().contains("1-2"));
}
    
    @Test (expected = JobException.class)
    public void shouldThrowExceptionWhenAddingCollectionOfLinkedJobs() throws Exception {
      instance.setJobNumber("1");
      MainJob subjob = new MainJob();
      subjob.setJobNumber("2");
      MainJob subjob1 = new MainJob();
      subjob1.setJobNumber("3");
      MainJob subjob2 = new MainJob();
      subjob2.setJobNumber("1-2");
      MainJob subjob3 = new MainJob();
      subjob3.setJobNumber("4");
      ArrayList<String> jobs = new ArrayList<>(
      Arrays.asList(
      subjob.getJobNumber(),subjob1.getJobNumber(),subjob2.getJobNumber(),subjob3.getJobNumber()
      )
      );
      instance.setLinkedJobs(jobs);
      
      
    }
    
    @Test 
    public void shouldAddCollectionOfLinkedJobs() throws Exception {
      instance.setJobNumber("1");
      MainJob subjob = new MainJob();
      subjob.setJobNumber("1-2");
      MainJob subjob1 = new MainJob();
      subjob1.setJobNumber("1-3");
      MainJob subjob2 = new MainJob();
      subjob2.setJobNumber("1-4");
      MainJob subjob3 = new MainJob();
      subjob3.setJobNumber("1-5");
      ArrayList<String> jobs = new ArrayList<>(
      Arrays.asList(
      subjob.getJobNumber(),subjob1.getJobNumber(),subjob2.getJobNumber(),subjob3.getJobNumber()
      )
      );
      instance.setLinkedJobs(jobs);
      
      assertTrue(instance.getLinkedJobs().contains("1-2"));
      assertTrue(instance.getLinkedJobs().contains("1-3"));  
      assertTrue(instance.getLinkedJobs().contains("1-4"));  
      assertTrue(instance.getLinkedJobs().contains("1-5"));  
      
    }
    
    
}

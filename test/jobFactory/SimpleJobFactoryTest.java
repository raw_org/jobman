/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jobFactory;


import JobTypes.BaseTypes.Job;
import JobTypes.BaseTypes.SpawnableJob;
import JobTypes.MainJob;
import java.util.Date;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author worralr
 */
public class SimpleJobFactoryTest{

SimpleJobFactory instance;
    
@Before
public void setup(){
  instance  = new SimpleJobFactory();  
    
}
   

    @Test
    public void shouldCreateJob() throws Exception {
        instance.setJobType("JobTypes.MainJob");
    Job aJob = instance.createJob();
        assertTrue(aJob instanceof Job);
    }
     @Test(expected = JobTypeNotValidException.class)
    public void shouldNotCreateJobShouldInsteadThrowException() throws Exception {
        instance.setJobType("Some Job");
        Job aJob = instance.createJob();
        assertTrue(aJob instanceof Job);
    }
    

@Test
    public void shouldCreateSpawnableJob_4args() throws Exception {
     instance.setSpawnableType("JobTypes.MainJobReplicator");
        Job aJob = new MainJob();
        Date aDate = new Date();
    SpawnableJob aSpawnableJob = instance.createSpawnableJob(aJob,aDate,false, 650000);
    assertTrue(aSpawnableJob instanceof SpawnableJob);
    assertThat(aSpawnableJob.getJobToBeReplicated(), equalTo(aJob));
    assertThat(aSpawnableJob.getDateToSpawn(), equalTo(aDate));
    assertThat(aSpawnableJob.getReplicationPeriod(), equalTo(650000L));
    assertThat(aSpawnableJob.spawnOnceCompleted(), equalTo(false));
    }

    @Test
    public void shouldCreateSpawnableJob() throws Exception {
       instance.setSpawnableType("JobTypes.MainJobReplicator");
        Job aJob = new MainJob();
    SpawnableJob aSpawnableJob = instance.createSpawnableJob(aJob);
    assertTrue(aSpawnableJob instanceof SpawnableJob);}
    
}

import java.awt.*;
import java.awt.geom.*;
import java.util.Calendar;
import java.util.GregorianCalendar;
import javax.swing.*;

/**
 * A analogous clock display. 
 *
 * GUI test class for the clockwork class from the exercise.
 */
public class Clock extends JComponent implements Runnable {
  
  /**
   * Useful for getting a math-like coordinate system (flips y).
   */
  private static final AffineTransform AFFINE_TRANSFORM =
    new AffineTransform(1.,0., 0.,-1., 0.,0.);

  /** Debug flag. */
  private static boolean debug_ = System.getProperty("debug") != null;

  /** The clockwork to be implemented in the exercise. */
  private Clockwork aClockwork_;
  /**
   * Thread for updating the clock.
   * @see run
   */
  private Thread anUpdateThread_;


  /**
   * Construcs a new clock for the given time. 
   */
  public Clock(int h, int m, int s) {
    aClockwork_ = new Clockwork(h, m, s);
    anUpdateThread_ = new Thread((Runnable) this, "clock updater");
    anUpdateThread_.start();
  }

  /**
   * Draws a clock needle, clocks center is supposed to be at (0,0).
   *
   * @param g the 2D graphics object to draw with.
   * @param l the length of the needle.
   * @param angle the angle of the needle, from 0.0 to 1.0, zero and
   *        correspond to the 12 o'clock position, parameter is going
   *        counterclockwise.
   */
  private void drawNeedle(Graphics2D g, double l, double angle) {
    Graphics2D _aGraphics2D = (Graphics2D) (g.create());
    // make the coordinate system math-like (swap y)
    _aGraphics2D.transform(AFFINE_TRANSFORM);
    // make it a math angle (in radians):
    double alpha = (1.0-angle) - 0.75;
    if (alpha < 0.0)
      alpha += 1.0;
    alpha *= 2*Math.PI;
    _aGraphics2D.rotate(alpha);
    _aGraphics2D.draw(new Line2D.Double(0., 0., l, 0));
    _aGraphics2D.dispose();
 }

  /**
   * Paints a very simple analogous clock for the time given be the
   * clockwork.
   */
  public void paintComponent(Graphics g) {
    Graphics2D _aGraphics2D = (Graphics2D) g;
    Dimension _aDimension = getSize();
    _aGraphics2D.setColor(Color.darkGray);
    g.fillRect(0, 0, _aDimension.width, _aDimension.height);
    int diam = Math.min(_aDimension.width, _aDimension.height);
    double r = diam / 2. - 1.;
    _aGraphics2D.translate(_aDimension.width/2, _aDimension.height/2);
    Ellipse2D _ellipse2D =
      new Ellipse2D.Double(-diam/2, -diam/2, diam-1, diam-1);
    _aGraphics2D.setColor(Color.white);
    _aGraphics2D.fill(_ellipse2D);
    double h = (double) aClockwork_.getHours();
    double m = (double) aClockwork_.getMinutes();
    double s = (double) aClockwork_.getSeconds();
    if (debug_)
      System.out.println(( (h<10.)? "0": "")+((int) h)
			 +((m<10.)?":0":":")+((int) m)
			 +((s<10.)?":0":":")+((int) s));
    // draw hours needle:
    _aGraphics2D.setColor(Color.black);
    _aGraphics2D.setStroke(new BasicStroke(4.F));
    drawNeedle(_aGraphics2D, 0.4*r,  h / 12.);
    // draw minutes needle:
    // as hours _aGraphics2D.setColor(Color.black);
    _aGraphics2D.setStroke(new BasicStroke(2.F));
    drawNeedle(_aGraphics2D, 0.9*r, m / 60.);
    // draw seconds needle:
    _aGraphics2D = (Graphics2D) (g.create());
    _aGraphics2D.setColor(Color.red);
    _aGraphics2D.setStroke(new BasicStroke(1.F));
    drawNeedle(_aGraphics2D, 0.95*r, s / 60.);
    _aGraphics2D.dispose();
  }

  /**
   * Make sure we haven't got a size of zero times zero pixels.
   */
  public Dimension getPreferredSize() {
    return new Dimension(100, 100);
  }

  /**
   * Make sure we haven't got a size of zero times zero pixels.
   */
  public Dimension getMinimumSize() {
    return new Dimension(100, 100);
  }

  /**
   * The run of the updating thread: makes the clockwork to advance every
   * second and triggers a clock repaint.
   */
  public void run() {
    for (long ts = System.currentTimeMillis(); true; ts+=1000) {
      long _sleeptime = ts+1000-System.currentTimeMillis();
      if (_sleeptime > 0) {
	try {
	  Thread.sleep(_sleeptime);
	} catch (InterruptedException e) {
	  System.err.println("aborting due to interrupt");
	  return;
	}
      }
      aClockwork_.stepSecond();
      if (isVisible()) 
	repaint();
    }
  }

  /**
   * Launches a framed clock.  
   */
  public static void main(String[] argv) {
    int h, m, s;
    if (argv.length == 0){
      // use current time
      GregorianCalendar _aGregorianCalendar = new GregorianCalendar();
      h = _aGregorianCalendar.get(Calendar.HOUR);
      m = _aGregorianCalendar.get(Calendar.MINUTE);
      s = _aGregorianCalendar.get(Calendar.SECOND);
    } else {
      try {
	// parse time from command line
	h = Integer.parseInt(argv[0]);
	m = Integer.parseInt(argv[1]);
	s = (argv.length>2) ? Integer.parseInt(argv[2]) : 0;
      } catch (Exception e) {
	System.err.println("usage> java Clock [<h> <m> [<s>]]");
	return;
      }
    }
    JFrame _aJFrame = new JFrame("clock");
    _aJFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    _aJFrame.getContentPane().add(new Clock(h, m, s));
    _aJFrame.pack();
    _aJFrame.setVisible(true);    
  }
}

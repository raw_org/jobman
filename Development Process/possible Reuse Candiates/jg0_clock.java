/////////////////////////////////////////////////////////////////////
// (C) Michael A Smith 1997-2000 University of Brighton            //
//     http://www.it.brighton.ac.uk/~mas                           //
//     From the book Java An Object-Oriented Language              //
/////////////////////////////////////////////////////////////////////
// Java 2 Application / Applet                                     //
//   Hence may not work with a JDK 1.0.1 / 1.1.* Compiler          //
// Version automatically created  Sat 10 Feb 2001 19:28:19 GMT  //
/////////////////////////////////////////////////////////////////////
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import java.io.IOException;
import java.util.*;
import java.text.*;
import java.util.*;

class Main
{
  public static void main( String args[] )
  {
     ClockDisplay app = new ClockDisplay();
     app.show();
  }
}

class WINDOW
{
  public static final int H = 200;
  public static final int W = 200;
}

class AnalogueClock extends Canvas implements Observer
{
  private Graphics  theAltGraphics;
  private Dimension theAltDimension;
  private Image     theAltImage;
  private int       theTime;

  private int theCurX      = WINDOW.W/2;
  private int theCurY      = WINDOW.H/2;
  private int theCurRadius = (theCurY-10)/2;


  public void update( Observable o, Object arg  )
  {
    Clock c = (Clock) o;                     // Observed Object
    theTime = c.getTime();                   // Get time
    repaint();                               // Re paint canvas
                                             // Will call update
  }

  public void paint ( Graphics g )           //First show or damaged
  {
    reDisplay( g );
  }

  public void update ( Graphics g )          //Update of view requested
  {
    reDisplay( g );
  }

  public void reDisplay( Graphics g )        //Re-text the object
  {
    Dimension d = getSize();                 //Size of image

    //See if image resized or first time

    if (  ( theAltGraphics == null )  ||
          ( d.width  != theAltDimension.width ) ||
          ( d.height != theAltDimension.height ) )
    {
      theAltDimension = d;
      theAltImage     = createImage( d.width, d.height );
      theAltGraphics  = theAltImage.getGraphics();

      theCurX      = d.width/2;
      theCurY      = d.height/2;
      theCurRadius = theCurX > theCurY
                             ? theCurY : theCurX;
    }

    theAltGraphics.setColor( Color.white );
    theAltGraphics.fillRect( 0, 0, d.width, d.height );

    int secs = theTime % 60;
    int mins = (theTime /60) % 60;
    int hrs  = (theTime /3600) % 24;

    theAltGraphics.setColor( Color.black );
    theAltGraphics.drawOval( theCurX-theCurRadius,
                             theCurY-theCurRadius,
                             theCurRadius*2, theCurRadius*2 );

    drawHand( secs*6, Color.red,   theCurRadius*18/20 );
    drawHand( mins*6, Color.blue,  theCurRadius*19/20 );
    drawHand( hrs*30, Color.black, theCurRadius*5/6 );

    g.drawImage( theAltImage, 0, 0, this );
  }

  // The Hand angle is expresed in degrees.

  private void drawHand( final int handAngle,
                         final Color c,   final int length )
  {
    double theta = ((double)handAngle-90.0) * (2.0 * Math.PI) /360.0;
    int toX = (int) ( theCurX + length * Math.cos( theta ) );
    int toY = (int) ( theCurY + length * Math.sin( theta ) );

    theAltGraphics.setColor( c );
    theAltGraphics.drawLine( theCurX, theCurY, toX, toY );
  }

}

class DigitalClock extends Canvas implements Observer
{
  private Graphics  theAltGraphics;
  private Dimension theAltDimension;
  private Image     theAltImage;
  private int       theTime;


  DigitalClock()
  {
    //SetSize( 1, 1 );
    repaint();
  }

  public void update( Observable o, Object arg  )
  {
    Clock c = (Clock) o;                     // Observed Object
    theTime = c.getTime();                   // Get time in seconds
    repaint();                               // Re paint canvas
                                             // Will call update
  }

  public void paint ( Graphics g )           // First show or damaged
  {
    reDisplay( g );                          // Common code
  }

  public void update ( Graphics g )          // Update of view requested
  {
    reDisplay( g );                          // Common code
  }

  public void reDisplay( Graphics g )        //Re-text the object
  {
    Dimension d       = getSize();           //Size of image

    //See if image resized or first time

    if (  ( theAltGraphics == null )  ||
          ( d.width  != theAltDimension.width ) ||
          ( d.height != theAltDimension.height ) )
    {
      theAltDimension = d;
      theAltImage     = createImage( d.width, d.height );
      theAltGraphics  = theAltImage.getGraphics();
    }

    Font font = new Font("Monospaced",Font.PLAIN,24);
    theAltGraphics.setFont( font );
    FontMetrics fm = theAltGraphics.getFontMetrics();

    theAltGraphics.setColor( Color.white );
    theAltGraphics.fillRect( 0, 0, d.width, d.height );

    theAltGraphics.setColor( Color.black );

    int secs = theTime % 60;
    int mins = (theTime / 60) % 60;
    int hrs  = (theTime / 3600) % 24;

    String time = ( hrs  <= 9 ? "0" : "" ) + hrs  + " : " +
                  ( mins <= 9 ? "0" : "" ) + mins + " : " +
                  ( secs <= 9 ? "0" : "" ) + secs;

    int strWidth = fm.stringWidth( time );
    theAltGraphics.drawString(time, (d.width-strWidth)/2, d.height-5);

    g.drawImage( theAltImage, 0, 0, this );
  }

}

class ClockDisplay extends JFrame
{
  private Clock    theClock = new Clock();   // Clock
  private Button   theStart;                 // Buttons (Visual)

  public ClockDisplay()
  {
    setSize( WINDOW.W, WINDOW.H );           // Size of WINDOW

    Panel panel = new Panel();               // Panel
    panel.setLayout( new BorderLayout() );

    ButtonAction cb = new ButtonAction();    // Call back
    theStart = new Button( "new clock" );
    theStart.addActionListener( cb );        // code

    AnalogueClock aAC = new AnalogueClock(); // Visual A. Clock
    DigitalClock  aDC = new DigitalClock();  // Visual D. Clock
    aDC.setSize( WINDOW.W, 40 );
    theClock.addObserver( aAC );             // Add as observer
    theClock.addObserver( aDC );             // Add as observer


    panel.add( "North",  aDC );              //
    panel.add( "Center", aAC );              //
    panel.add( "South",  theStart );         //
    getContentPane().add( panel );           //To window

    setTitle("Clock");                       //Title
  }

  class ButtonAction implements ActionListener
  {
    public void actionPerformed( ActionEvent event )
    {
      theClock.kill();                       // Old Clock
      theClock = new Clock();
    }
  }

}


class Clock extends Observable implements Runnable
{
  private int     theTime;                   //Clock's time
  private boolean theLifeState = true;
  private Thread  theTicks;

  public Clock()
  {
    theTime = 0;
    theTicks = new Thread( this );
    theTicks.start();
  }

  public synchronized int getTime()
  {
    return theTime;
  }

  public synchronized void setTime( final int time )
  {
    theTime = time;
  }

  public void kill()
  {
    theLifeState = false;
  }

  public void run()
  {
    while ( theLifeState )
    {
      GregorianCalendar calendar = new GregorianCalendar();
      int secs = calendar.get( Calendar.HOUR )   * 3600 +
                 calendar.get( Calendar.MINUTE ) * 60 +
                 calendar.get( Calendar.SECOND );
      try
      {
        setTime( secs );
        Thread.sleep( 1000 );
        setChanged();
        notifyObservers();
      }
      catch ( InterruptedException err )
      {
      }
    }
  }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package stopwatch;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.JComponent;
import javax.swing.JFrame;

/**
 *
 * @author Prime - Work
 */
public class ResetButton extends JComponent implements MouseListener{

    private int innerCircleSize;
    private int outerCircleSize;
    private Font labelFont = new Font("Serif", Font.BOLD, 20);
    private boolean pressed = false;

    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
          JFrame world  =  new JFrame();
          ResetButton JComponent = new ResetButton();
          //JComponent.setEnabled(false);
          world.add(JComponent);
          world.setVisible(true);
            }
        });
    }



    public ResetButton() {
        super();
        addMouseListener(this);
        setSize(10,10);

    }

    public void paintComponent(Graphics g){
        
        //get a copy of the grphics object and cast it to a Graphics2D object
        Graphics2D g2d = (Graphics2D) g.create();
        //turn on antialiasing
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        //if the button is pressed fill the outercirle

        if (pressed && isEnabled()){

        g2d.setColor(Color.RED);
        g2d.fillOval(5, 5, 30, 30);
        g2d.setColor(Color.BLACK);
        g2d.setFont(labelFont);
        g2d.drawString("R",14,26);
        }else{
            //if the button is not enabled then
            if(isEnabled() == false){
            //Graphics2D g2d = (Graphics2D)g;
       AlphaComposite  newComposite = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, .5f);
       g2d.setComposite(newComposite);
            }
        //clear the space for redrawing
        //g2d.clearRect(5, 5, 30, 30);
        g2d.setColor(Color.RED);

        //draw the outer circle
        g2d.drawOval(5, 5, 30, 30);
        //draw a filled red circle
        
        g2d.fillOval(10, 10, 20 ,20);
        //draw the Letter R in the middle
        g2d.setColor(Color.BLACK);
        g2d.setFont(labelFont);
        g2d.drawString("R",14,26);
        }

    }

    @Override
    public void mouseClicked(MouseEvent e) {

    }

    @Override
    public void mousePressed(MouseEvent e) {
        pressed = true;
        this.repaint();
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        pressed = false;
        this.repaint();
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    this.repaint();
    }

    @Override
    public void mouseExited(MouseEvent e) {
      this.repaint();
    }

    



}


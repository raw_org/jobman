/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * SwatchPanel2.java
 *
 * Created on 27-Feb-2009, 11:49:01
 */

package stopwatch;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.LayoutManager;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Area;
import java.awt.geom.Ellipse2D;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.Timer;

/**
 *
 * @author Prime - Work
 */
public class SwatchPanel2 extends javax.swing.JPanel implements ActionListener {
    //variables to draw the stopwatch
    private Color outerCircleColour = Color.BLACK;
    private Color lineColor = Color.BLACK;
    private Color segmentColor = Color.BLUE;
    private Color numberColor = Color.RED;
    private Color labelColor = Color.BLUE;
    private Font labelFont = new Font("Serif", Font.BOLD, 20);
    private int labelFontSize = 20;
    private Font numberFont = new Font("Serif", Font.BOLD,22);
    private int numberFontSize = 22 ;
    private boolean displayWorkingHours = false;
    
    private long numberOfWorkingHoursInDayinSeconds = 7 * 3600;

        

    public boolean isDisplayWorkingHours() {
        return displayWorkingHours;
    }

    public void setDisplayWorkingHours(boolean displayWorkingHours) {
        this.displayWorkingHours = displayWorkingHours;
    }

       
    public Color getLabelColor() {
        return labelColor;
    }

    public void setLabelColor(Color labelColor) {
        this.labelColor = labelColor;
    }

    public Font getLabelFont() {
        return labelFont;
    }

    public void setLabelFont(Font labelFont) {
        this.labelFont = labelFont;
    }

    public int getLabelFontSize() {
        return labelFontSize;
    }

    public void setLabelFontSize(int labelFontSize) {
        this.labelFontSize = labelFontSize;
    }

    public Color getLineColor() {
        return lineColor;
    }

    public void setLineColor(Color lineColor) {
        this.lineColor = lineColor;
    }

    public Color getNumberColor() {
        return numberColor;
    }

    public void setNumberColor(Color numberColor) {
        this.numberColor = numberColor;
    }

    public Font getNumberFont() {
        return numberFont;
    }

    public void setNumberFont(Font numberFont) {
        this.numberFont = numberFont;
    }

    public int getNumberFontSize() {
        return numberFontSize;
    }

    public void setNumberFontSize(int numberFontSize) {
        this.numberFontSize = numberFontSize;
    }

    public Color getOuterCircleColour() {
        return outerCircleColour;
    }

    public void setOuterCircleColour(Color outerCircleColour) {
        this.outerCircleColour = outerCircleColour;
    }

    public Color getSegmentColor() {
        return segmentColor;
    }

    public void setSegmentColor(Color segmentColor) {
        this.segmentColor = segmentColor;
    }
    // variables representing the time.
    
    private long days = 0;
    private long hours = 0;
    private long minutes = 0;
    private long second = 0;
//the model variable

    private SwatchModel model;
//the variables for animated time
    private long animatedTime;
    private Timer animationTimer = new Timer(1000, this);
    private boolean startStopWatchOnOpening = false;

    private boolean running;//this tell the swing timer if the animation should update the UI and external components that stopwatch is running

    public boolean isRunning() {
        return running;
    }
    /** Creates new form SwatchPanel2 */
    public SwatchPanel2() {
        initComponents();
        model = new SwatchModel();
        //animationTimer.setDelay(1000);
        //animationTimer.setCoalesce(true);
        //animationTimer.start();
        
    }

    public SwatchPanel2(LayoutManager layout) {
        super(layout);
    }

    public SwatchPanel2(LayoutManager layout, boolean isDoubleBuffered) {
        super(layout, isDoubleBuffered);
    }


    public SwatchPanel2(boolean isDoubleBuffered) {
        super(isDoubleBuffered);
    }

    public long getAllottedTime(){
    return model.getAllottedTime();
    }

    public void setAllottedTime(long time){
    model.setAllottedTime(time);
    setAllottedTimeLabels(time);
    }

    public boolean isStartStopWatchOnOpening() {
        return startStopWatchOnOpening;
    }

    public void setStartStopWatchOnOpening(boolean startStopWatchOnOpening) {
        this.startStopWatchOnOpening = startStopWatchOnOpening;
    }



    //main method
    public static void main(String[] args) {
        SwatchPanel2 content = new SwatchPanel2();
        JFrame window = new JFrame("GUI Test");
      window.setContentPane(content);
      window.setSize(200,200);
      window.setLocation(0,0);
      window.setVisible(true);
    }

    private void drawLabels(Graphics2D g, double windowWidth, double windowHeight) {
      //create a copy of the graphics object
        Graphics2D g2d = (Graphics2D) g.create();
        //set the color
        g2d.setColor(labelColor);
        //set the font by first see if the font has changed
        if (labelFont != null && labelFont != g2d.getFont()){
        // if the font has changed set the font
        g2d.setFont(labelFont);
        }
        //declar some variables to place the labels with
      Double x;
      Double y;
      //set the x and y for the Day label
      x = windowWidth * 0.12;
      y = windowHeight * 0.27;
      //draw the day label
      g2d.drawString("DAY",x.intValue(), y.intValue());
      //set the x and y for the hour
      x = windowWidth * 0.69;
      y = windowHeight * 0.27;
      //draw the hour label
      g2d.drawString("HRS",x.intValue(), y.intValue());
      //set the x and y for the minute label
      x = windowWidth * 0.12;
      y = windowHeight * 0.67;
      //draw the minute label
      g2d.drawString("MIN",x.intValue(), y.intValue());
      //set the x and y for the sec label
      x = windowWidth * 0.69;
      y = windowHeight * 0.67;
      g2d.drawString("SEC",x.intValue(), y.intValue());
    }

    private void drawNumbers(Graphics g, double windowWidth, double windowHeight) {
      Graphics2D g2d = (Graphics2D) g.create();
        //set the color
        g2d.setColor(numberColor);
        //see if the font has been set
        if (numberFont != null && numberFont != g2d.getFont()){
        // if the font has changed set the font
        g2d.setFont(numberFont);
        }
      //declare some variables to place the numbers with
      Double x;
      Double y;
      // the units have off sets to place the number directly under the middle letter of its lable
      double lessThan10Offset = 0.07;
      double moreThan10Offset = 0.03;
      double moreThan100Offset = 0.01;
      double offSet;
      //work out the offset for day figure
      if(days < 10){
      offSet = lessThan10Offset;
      }
      else{
          if(days >=10 & days < 100){
          offSet = moreThan10Offset;
          }
          else{
          offSet = moreThan100Offset;
          }
      }
      //set the x and y for the Day figures.
      x = windowWidth * (0.12 + offSet);
      y = windowHeight * 0.37;
      //draw the days value
      g2d.drawString("" + days, x.intValue(), y.intValue());
      //work out the offset for hour figure
      if(hours < 10){
      offSet = lessThan10Offset;
      }
      else{
          if(hours >=10 & hours < 100){
          offSet = moreThan10Offset;
          }
          else{
          offSet = moreThan100Offset;
          }
      }
      //set the x and y for the Day figures.
      x = windowWidth * (0.69 + offSet);
      y = windowHeight * 0.37;
      //draw the days value
      g2d.drawString("" + hours, x.intValue(), y.intValue());
      //work out the offset for day figure
      if(minutes < 10){
      offSet = lessThan10Offset;
      }
      else{
          if(minutes >=10 & minutes < 100){
          offSet = moreThan10Offset;
          }
          else{
          offSet = moreThan100Offset;
          }
      }
      //set the x and y for the Day figures.
      x = windowWidth * (0.12 + offSet);
      y = windowHeight * 0.77;
      //draw the min value
      g2d.drawString("" + minutes, x.intValue(), y.intValue());
      //work out the offset for second figure
      if(second < 10){
      offSet = lessThan10Offset;
      }
      else{
          if(second >=10 & second < 100){
          offSet = moreThan10Offset;
          }
          else{
          offSet = moreThan100Offset;
          }
      }
      //set the x and y for the Day figures.
      x = windowWidth * (0.69 + offSet);
      y = windowHeight * 0.77;
      //draw the days value
      g2d.drawString("" + second, x.intValue(), y.intValue());
    }





    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        resetButton1 = new stopwatch.ResetButton();
        startStopButton1 = new RAWCustomSwingComponents.StartStopButton();
        resetButton2 = new stopwatch.ResetButton();

        javax.swing.GroupLayout resetButton1Layout = new javax.swing.GroupLayout(resetButton1);
        resetButton1.setLayout(resetButton1Layout);
        resetButton1Layout.setHorizontalGroup(
            resetButton1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );
        resetButton1Layout.setVerticalGroup(
            resetButton1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );

        startStopButton1.setBorder(null);
        startStopButton1.setText("startStopButton1");
        startStopButton1.setAlignmentX(0.5F);
        startStopButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                startStopButton1ActionPerformed(evt);
            }
        });

        resetButton2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                resetButton2MouseClicked(evt);
            }
        });

        javax.swing.GroupLayout resetButton2Layout = new javax.swing.GroupLayout(resetButton2);
        resetButton2.setLayout(resetButton2Layout);
        resetButton2Layout.setHorizontalGroup(
            resetButton2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 40, Short.MAX_VALUE)
        );
        resetButton2Layout.setVerticalGroup(
            resetButton2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 42, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(72, 72, 72)
                        .addComponent(startStopButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 57, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(81, 81, 81)
                        .addComponent(resetButton2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(71, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(67, Short.MAX_VALUE)
                .addComponent(startStopButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(31, 31, 31)
                .addComponent(resetButton2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void setAllottedTimeLabels(long time) {
    // System.out.println("setting Labels");
    //TODO: Setup so that the number of working hours can be changed
        if (displayWorkingHours){

    long ddr = time % numberOfWorkingHoursInDayinSeconds;
    hours =  ddr / 3600;
    long hhr = ddr % 3600;
    minutes =  hhr / 60;
    second = hhr % 60;
        }
        else {
        time = time/1000;
    days =  time / 86400;
    long ddr = time % 86400;
    hours =  ddr / 3600;
    long hhr = ddr % 3600;
    minutes =  hhr / 60;
    second = hhr % 60;
        }
    
    repaint();
    }
   

    private void startStopButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_startStopButton1ActionPerformed

        if (startStopButton1.isSelected()){
            resetButton2.setEnabled(false);
            startSWatch();
        } else {
            resetButton2.setEnabled(true);
            stopSWatch();
            
        }
        
    }//GEN-LAST:event_startStopButton1ActionPerformed

    private void resetButton2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_resetButton2MouseClicked
                if (model.isRunning() == false){
            int confirmReset = JOptionPane.showConfirmDialog(this,"Are you sure you want to reset the Stopwatch");
            if (confirmReset == 0){
                animatedTime = 0;
                model.setAllottedTime(0);
                setAllottedTimeLabels(model.getAllottedTime());
            }
        }

    }//GEN-LAST:event_resetButton2MouseClicked


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private stopwatch.ResetButton resetButton1;
    private stopwatch.ResetButton resetButton2;
    private RAWCustomSwingComponents.StartStopButton startStopButton1;
    // End of variables declaration//GEN-END:variables


    //custom paintComponent method

    @Override
    public void paintComponent(Graphics g){
 //create a copy of the graphics object and cast it as a Graphics2D object
        super.paintComponent(g);
        Graphics2D g2d = (Graphics2D) g.create();
 // turn on antialiasing
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
//draw the outer circle
        //set to the colour of the outer circle
       g2d.setColor(outerCircleColour);
       //we want the oval to be a little smaller (2%)than the panel to ensure it is all drawn 
       Double ovalWidth = getWidth() * 0.99;
       Double ovalHeight = getHeight() * 0.99;
       //now draw the outer circle (oval)
       g2d.drawOval(0, 0, ovalWidth.intValue(),ovalHeight.intValue());
//draw the inner lines
       g2d.setColor(lineColor);
       g2d.drawLine(getWidth()/2, 0, getWidth()/2, getHeight());
       g2d.drawLine(0, getHeight()/2, getWidth(), getHeight()/2);
//draw the segments
       //work out the postition of the segments
       double positionX; // position where the segments will be drawn on X
       double positionY; // position where the segenets will be drawn on Y
       double outerSize; // the outer edge circumferece of the segments
       double innerSize; // the inner edge circumferece of the segments
       double windowWidth = new Double(getWidth());// the width of the window
       double windowHeight = new Double(getHeight()); // the height of the window
       double segmentScaleX = 0.1;// this is position X of the segment to the size of the component
       double segmentScaleY = 0.1;// this is position Y of the segment to the size of the component
       double segmentOuterSize = 0.8; // this is the scale for the circumference of the outer edge of the segments
       double segmentInnerSize = 0.7; // this is the scale for  the circumference of the inner edge of the segmets
       //work out the posistion of the segments
       positionX = windowWidth * segmentScaleX;
       positionY = windowHeight * segmentScaleY;
       //work out the size of the inner and outer edges
       outerSize = windowWidth * segmentOuterSize;
       innerSize = windowHeight * segmentInnerSize;
       //draw the segements using the segmentGenerator
       g2d.setColor(segmentColor);
       g2d.fill(getSegments(positionX,positionY, innerSize, outerSize, windowWidth, windowHeight));
//draw the figures D H M S
        drawLabels(g2d, windowWidth, windowHeight);
//draw the numbers
        drawNumbers(g2d, windowWidth, windowHeight);
//draw the other components
  //super.paintComponent(g);

    }

    private Shape getSegments(double x, double y, double innerRadius, double outerRadius, double windowWidth, double windowHeight){
 Area a1 = new Area(
                new Ellipse2D.Double(x,y,outerRadius,outerRadius)
                );
        double innerOffset = (outerRadius - innerRadius)/2;

        Area a2 = new Area(
                new Ellipse2D.Double(x+innerOffset,y+innerOffset,innerRadius,innerRadius)
                );
        a1.subtract(a2);
        //now remove spaces to make the segments these are rectangular spaces.
        //create 2 coordinate posistions to cut out the segments
        Double boxX;
        Double boxY;
        //create the size of the cutouts
        Double boxWidth = windowWidth * 0.9;
        Double boxHeight = windowHeight * 0.2;
        //work out the coordinate for the first cutout
        boxX = windowWidth * 0.1;
        boxY = windowHeight * 0.2;
        Area break1 = new Area(
                new Rectangle(boxX.intValue(),boxY.intValue(),boxWidth.intValue(),boxHeight.intValue())
                );
        a1.subtract(break1);
        //work out the y coordinate for the second cut out
        boxY = windowHeight * 0.6;
        //create the second break
        Area break2 = new Area(
                new Rectangle(boxX.intValue(),boxY.intValue(),boxWidth.intValue(),boxHeight.intValue())
                );
        a1.subtract(break2);
        //return the segments
        return a1;
    }



 public void startSWatch() {
    setAllottedTimeLabels(model.getAllottedTime());
    animatedTime = model.getAllottedTime();
    model.startSWatch();
    running = true;
    animationTimer.start();
    if(startStopButton1.isSelected()){
    //if the start stop button is selected do nothing
    }
    else{
    startStopButton1.setSelected(true);
    }
    }

    public void stopSWatch() {
    model.stopSWatch();
    running = false;
    setAllottedTimeLabels(model.getAllottedTime());
    animationTimer.stop();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        
        if (running)
        {
        //System.out.println("animation timer called");
        animatedTime = animatedTime + 1000;
        setAllottedTimeLabels(animatedTime);
        
        }
    }




 


}

/*
 * Swatch.java
 *
 * Created on 12 July 2007, 10:24
 */

package stopwatch;

import javax.swing.JOptionPane;

/**
 *
 * @author  Robert worrall
 */
public class Swatch extends javax.swing.JDialog implements Runnable{
    
   private SwatchModel model;
   private String timeString = "";
   private boolean runThread = true;
   private boolean running = false;
   private long tempTime = 0;
   private Thread t;
   
    /** Creates new form Swatch */
   
    public Swatch(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        model = new SwatchModel();
        t = new Thread(this, "StopWatch Thread");
        t.start();
        //System.out.println("Thread Started" + t.isAlive());
        }
    
    public Swatch(long time){
        initComponents();
        setAllottedTime(time);
    }
    
    public long getAllottedTime(){
    return model.getAllottedTime();
    }
    
    public void setAllottedTime(long time){
    model.setAllottedTime(time);
    allottedTimeLabel.setText(formatTime(time));
    }
    
    public void startSWatch(){
    tempTime = model.getAllottedTime();    
    timeString = formatTime(tempTime);
    allottedTimeLabel.setText(timeString);
    model.startSWatch();
    running = true;
    
    }
    
    public void stopSWatch(){
    model.stopSWatch();
    timeString = formatTime(model.getAllottedTime());
    allottedTimeLabel.setText(timeString);
    running = false;
    }
    
    private String formatTime(long time){
    String result;
    time = time/1000;
    String daySpacer = "";
    String hourSpacer = "";
    String minuteSpacer = "";
    String secondSpacer = "";
    long dd =  time / 86400;
    long ddr = time % 86400;
    long hh =  ddr / 3600;
    long hhr = ddr % 3600;
    long mm =  hhr / 60;
    long ss = hhr % 60;
    
    if(dd <= 9){
        daySpacer = "0";
    }
    if(hh <= 9){
        hourSpacer = "0";
    }
    if(mm <= 9){
        minuteSpacer = "0";
    }
    if(ss <= 9){
        secondSpacer = "0";
    }
    
    result = daySpacer + dd + " : " + hourSpacer + hh + " : " + minuteSpacer + mm + " : " + secondSpacer + ss;
    return result;
    }
    
     public void run(){
     while(runThread == true){     
     while (running == true){
         tempTime += 1000;
     timeString = formatTime(tempTime);
     allottedTimeLabel.setText(timeString);
          try{
        Thread.sleep(1000);
     }
     catch (InterruptedException e){}
     }
     }
     try{
        Thread.sleep(1000);
     }
     catch (InterruptedException e){}
     }
    
    
    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc=" Generated Code ">//GEN-BEGIN:initComponents
    private void initComponents() {
        allottedTimeLabel = new javax.swing.JLabel();
        startStopToggleButton = new javax.swing.JToggleButton();
        resetButton = new javax.swing.JButton();
        TimeLabel = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        allottedTimeLabel.setFont(new java.awt.Font("Arial", 1, 26));
        allottedTimeLabel.setText("00 : 00 : 00 : 00");

        startStopToggleButton.setFont(new java.awt.Font("Tahoma", 0, 14));
        startStopToggleButton.setText("Start / Stop");
        startStopToggleButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                startStopToggleButtonActionPerformed(evt);
            }
        });

        resetButton.setFont(new java.awt.Font("Tahoma", 0, 14));
        resetButton.setText("Reset");
        resetButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                resetButtonActionPerformed(evt);
            }
        });

        TimeLabel.setFont(new java.awt.Font("Arial", 1, 24));
        TimeLabel.setText("DD : HH : MM : SS");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addComponent(allottedTimeLabel))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(startStopToggleButton, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(resetButton, javax.swing.GroupLayout.DEFAULT_SIZE, 74, Short.MAX_VALUE))
                    .addComponent(TimeLabel))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(TimeLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(allottedTimeLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(startStopToggleButton)
                    .addComponent(resetButton))
                .addContainerGap())
        );
        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void resetButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_resetButtonActionPerformed

        if (running == false){
        int confirmReset = JOptionPane.showConfirmDialog(this,"Are you sure you want to reset the Stopwatch"); 
        if (confirmReset == 0){
        tempTime = 0;
        model.setAllottedTime(0);
        timeString = formatTime(model.getAllottedTime());
        allottedTimeLabel.setText(timeString);
        }
        }
    }//GEN-LAST:event_resetButtonActionPerformed

    private void startStopToggleButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_startStopToggleButtonActionPerformed

        if (startStopToggleButton.isSelected()){
        startSWatch();
        }
        else {
        stopSWatch();
        }
    }//GEN-LAST:event_startStopToggleButtonActionPerformed
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Swatch(new javax.swing.JFrame(), true).setVisible(true);
            }
        });
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel TimeLabel;
    private javax.swing.JLabel allottedTimeLabel;
    private javax.swing.JButton resetButton;
    private javax.swing.JToggleButton startStopToggleButton;
    // End of variables declaration//GEN-END:variables
    
}

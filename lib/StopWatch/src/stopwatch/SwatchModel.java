/*
 * SwatchModel.java
 *
 * Created on 12 July 2007, 11:38
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package stopwatch;

import java.util.Date;

/**
 *
 * @author Robert worrall
 */
public class SwatchModel{
    
   private long startTime = 0;
   private long stopTime = 0;
   private long allottedTime = 0;
   private boolean running = false;
   
    
    /** Creates a new instance of SwatchModel */
    public SwatchModel() {
       
    }

    public boolean isRunning() {
        return running;
    }
    
    public long getAllottedTime(){
    return allottedTime;
    }
    
    public void setAllottedTime(long time){
    allottedTime = time;
    }
    
    public void startSWatch(){
    startTime = new Date().getTime();
    running = true;

    }
    
    public void stopSWatch(){    
    stopTime = new Date().getTime();
    allottedTime = allottedTime += (stopTime - startTime);
    running = false;
    }
    
        
}

package jobman;

/*
 * JobSorter.java
 *
 * Created on 23 September 2007, 18:51
 *
* Copyright (c) <2008>, <Robert Worrall>
 *  All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
    * Neither the name of the <ORGANIZATION> nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/



//import com.db4o.ObjectSet;
import JobTypes.MainJob;
import jobman.database.JobDataBaseDB4OEmbedded;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.Set;

/**
 *
 * @author Robert Worrall
 */
public class JobSorter {

    private JobDataBaseDB4OEmbedded jobDataBase;

    /** Creates a new instance of JobAlertSorter */
    public JobSorter() {
    }
    
    public ArrayList <MainJob> sortJobsDueNow(ArrayList <MainJob> jobList){          
        ArrayList <MainJob> result = new ArrayList();
        Iterator <MainJob> iterator = jobList.iterator();// use an iterator as jobListMaychangeDuringProcessing
        while (iterator.hasNext()){        
       MainJob currentJob = iterator.next();   
        
        if (checkIfDue(currentJob,new Date())){
         result.add(currentJob);
        }
        if(!currentJob.getLinkedJobs().isEmpty()){
             return mergeJobLists(result, sortJobsDueInTheNextHour(currentJob.getLinkedJobs()));
        }
        
        }
    return result;
    }
    
        
    private boolean checkIfDue(MainJob aJob, Date dateDue){   
         if( aJob.getReminder() != null){
             return aJob.getReminder().before(dateDue);
    }
    return false;
    }
    
    public ArrayList <MainJob> sortJobsDueInTheNextHour(ArrayList <String> jobList){        
        ArrayList <MainJob> result = new ArrayList();
        Iterator <String> iterator = jobList.iterator();
        while (iterator.hasNext()){
        MainJob currentJob = jobDataBase.findJobByJobNumber(iterator.next());   
        if (checkIfDue(currentJob, getDateInAnHour())){
         result.add(currentJob);
        }
        ArrayList <String> subJobList = currentJob.getLinkedJobs();
        if(subJobList.isEmpty()){
        }
        else{
        result = mergeJobLists(result, sortJobsDueInTheNextHour(subJobList));
        }
        
        }
    
    return result;   
    }

    public ArrayList <MainJob> sortJobsDueInTheNextHour(Set <MainJob> mainJobList) {
    ArrayList <MainJob> result = new ArrayList();     
    Iterator <MainJob> iterator = mainJobList.iterator();   
        while (iterator.hasNext()){
      MainJob currentJob = iterator.next();
        if (checkIfDue(currentJob, getDateInAnHour())){
         result.add(currentJob);
        }
        if(currentJob.getLinkedJobs().isEmpty()){
        }
        else{
        result = mergeJobLists(result, sortJobsDueInTheNextHour(currentJob.getLinkedJobs()));
        }
        }
    return result;  
    }

    private Date getDateInAnHour() {
        Date inAnHour = new Date(new Date().getTime() + 60000);
        return inAnHour;
    }
    
    private ArrayList <MainJob> mergeJobLists(ArrayList <MainJob> listOfJobs1, ArrayList <MainJob> listOfJobs2){
   
            listOfJobs2.stream().forEach(cnsmr -> listOfJobs1.add(cnsmr));
            
            return listOfJobs1;
    }


}

package jobman.database;

/*
 * JobDataBaseDB4OEmbedded.java
 *
 * Created on 27 September 2006, 17:36
 *
 * Copyright (c) <2008>, <Robert Worrall>
 *  All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
    * Neither the name of the <ORGANIZATION> nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
//import com.db4o.Null;
import jobman.database.interfaces.SpawnableJobDatabase;
import jobman.database.interfaces.EmbeddedJobDatabase;
import JobTypes.BaseTypes.Job;
import com.db4o.ObjectContainer;
import com.db4o.config.EmbeddedConfiguration;
import com.db4o.query.Predicate;
import com.db4o.Db4oEmbedded;
import java.util.List;
import java.util.ArrayList;
import com.db4o.ObjectSet;
import com.db4o.ext.DatabaseClosedException;
//import com.sun.org.apache.xml.internal.security.signature.ObjectContainer;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import jobman.DBconfig;
import JobTypes.MainJob;
import JobTypes.MainJobReplicator;
import JobTypes.BaseTypes.SpawnableJob;
import jobman.database.jobnumberingschemes.JobNumberingSchemeInterface;

//import javax.sql.rowset.Predicate;
/**
 *
 * @author Prime - Work
 */
public class JobDataBaseDB4OEmbedded implements EmbeddedJobDatabase, SpawnableJobDatabase {

    ObjectContainer currentDataBase;
    DBconfig dbConfig;
    private JobNumberingSchemeInterface numberingScheme;
    private EmbeddedConfiguration db4oConfig;
    private final JobBuffer buffer;

    /**
     * Creates a new instance of JobDataBase
     */
    public JobDataBaseDB4OEmbedded() {
        this.buffer = new JobBuffer();
        buffer.setMaximumCapacity(500);       
    }

    @Override
    public void loadDatabase(String filename) {

        db4oConfig = configureDatabase(db4oConfig);

        currentDataBase = Db4oEmbedded.openFile(db4oConfig, filename);// DB40 7.11 open file

        checkDBconfig(currentDataBase.query(DBconfig.class));
        numberingScheme = dbConfig.getNumberingSchemeImplementation();
    }

    private void checkDBconfig(List<DBconfig> configList) {
        if (configList.isEmpty()) {
            // no dbconfig create a new one
            System.out.println("Basic database configuration is missing!");
        } else {                  
                dbConfig = configList.get(0); 
                
        }
    }

    private EmbeddedConfiguration configureDatabase(EmbeddedConfiguration config) {
        //db4oConfig = Db4o.newConfiguration();
        config = Db4oEmbedded.newConfiguration();
        //allow the older versions of the data base to be loaded.
        config.common().allowVersionUpdates(true);
        //The next  set of lines of code make sure that when an object is updated in the DB4O databse all it's linked objects are also updated.
        config.common().activationDepth(2);
        config.common().objectClass(DBconfig.class).cascadeOnUpdate(true);
        config.common().objectClass(DBconfig.class).cascadeOnDelete(true);
        config.common().objectClass(MainJob.class).cascadeOnDelete(true);
        return config;
    }

    @Override
    public void createDatabase(String filename, String numberingScheme) throws DataBaseCreationOrOpeningException {
        dbConfig = new DBconfig();
        try {
        this.numberingScheme = (JobNumberingSchemeInterface)Class.forName(numberingScheme).newInstance();
        db4oConfig = configureDatabase(Db4oEmbedded.newConfiguration());
        currentDataBase = Db4oEmbedded.openFile(db4oConfig, filename);
        dbConfig.setNumberingSchemeImplementation(this.numberingScheme);
        currentDataBase.store(dbConfig);
        currentDataBase.close();
            } catch (ClassNotFoundException | InstantiationException | IllegalAccessException ex) {
                throw new DataBaseCreationOrOpeningException(ex.toString());
            }

    }

    @Override
    public void closeDatabase() {

        if (currentDataBase == null) {
            //System.out.println("current Databaase is set to null");
        } else {
            try {
                ////System.out.println("catagories are" + dbConfig.getCatagoryList().toString());
                currentDataBase.store(dbConfig);
                //currentDataBase.set(dbConfig);//old dB4o config command
                dbConfig = null;
                //System.out.println("closing database");
                currentDataBase.close();
                currentDataBase = null;
            } catch (DatabaseClosedException e) {
            }

        }

    }

    @Override
    public Set<MainJob> getAllCurrentMainJobs() {

        ObjectSet<MainJob> result = currentDataBase.query(new Predicate<MainJob>() {

            @Override
            public boolean match(MainJob mainJob) {
                return mainJob.getPercentComplete() < 100 && isATopLevelJob(mainJob.getJobNumber());
            }

            private boolean isATopLevelJob(String jobNumber) {

                return !jobNumber.contains("-");

            }

        });

        return new HashSet(result);

    }

    @Override
    public Set<Job> getAllFinshedMainJobs() {

        ObjectSet<MainJob> result = currentDataBase.query(new Predicate<MainJob>() {

            @Override
            public boolean match(MainJob mainJob) {
                return mainJob.getPercentComplete() == 100 && isATopLevelJob(mainJob.getJobNumber());
            }

            private boolean isATopLevelJob(String jobName) {
                boolean topLevel = false;
                //set up pattern match here  

                if (jobName.contains("-")) {
                    topLevel = false;
                } else {
                    topLevel = true;
                }
                return topLevel;
            }
        });
        return new HashSet<>(result);
    }

    @Override
    public synchronized void storeJob(Job aJob) {
        //System.out.println("Storing " + aJob.getJobName());
        currentDataBase.store(aJob);
        //currentDataBase.set(aJob); //old DB4O command
    }

    @Override
    public synchronized void deleteJob(Job aJob) {
        //remove the job from the buffer.
        buffer.removeJob(aJob.getJobNumber());
        //System.out.println("deleting job " + aJob.getJobName());
        currentDataBase.delete(aJob);

    }

    @Override
    public synchronized String issueJobNumber() {   
        String newJobNumber = numberingScheme.issueJobMainNumber(dbConfig.getLastJobNumber());
        dbConfig.setLastJobNumber(Integer.parseInt(newJobNumber));
        currentDataBase.store(dbConfig);
        //currentDataBase.set(dbConfig);//old dB4o config command
        return newJobNumber;
    }

    @Override
     public String issueSubJobNumber(String jobNumber, int size) {
    return numberingScheme.issueSubJobNumber(jobNumber, size);
    }
    
    @Override
    public ArrayList<String> getCatagoryList() {
        return dbConfig.getCatagoryList();
    }

    @Override
    public Set<SpawnableJob> getReplicatableJobs() {
        if (currentDataBase != null) {
            ObjectSet<SpawnableJob> result = currentDataBase.query(new Predicate<SpawnableJob>() {

                @Override
                public boolean match(SpawnableJob repJob) {
                    return true;
                }
            });
            return new HashSet(result);
        } 
        return null;
    }

    @Override
    public void storeReplicatableJob(SpawnableJob aJob) {
        currentDataBase.store(aJob);


    }

    @Override
    public void deleteReplicableJob(SpawnableJob aJob) {
        currentDataBase.delete(aJob);
    }

    @Override
    public Set<Job> findJob(final String searchString) {
        //create the ObjectSet to hold the results
        ObjectSet<Job> result = currentDataBase.query(new Predicate<Job>() {

            @Override
            public boolean match(Job mainJob) {
                boolean aMatch = false;
                if (mainJob.getName().contains(searchString)) {
                    return true;
                } else {
                    if (mainJob.getNotes().contains(searchString)) {
                        return true;
                    } else {
                        return mainJob.getOtherDetails().contains(searchString);
                    }
                }

            }
        });

        return new HashSet(result);
    }

    @Override
    public MainJob findJobByJobNumber(final String jobNumber) {
        MainJob result;
        if (buffer.jobBuffered(jobNumber)) {
            result = buffer.getJob(jobNumber);
        } else {
            //System.out.println("job not buffered retrieving job from database.");
            final ObjectSet<MainJob> searchResult = currentDataBase.query(new Predicate<MainJob>() {

                @Override
                public boolean match(MainJob mainJob) {
                    
                    return mainJob.getJobNumber().equals(jobNumber); //System.out.println("Job does Not match " + mainJob.getJobNumber() + " to " + jobNumber);
                    
                }

            });
            if (searchResult.isEmpty()) {
                result = null;
            } else {
                result = searchResult.get(0);
                //place result into the buffer
                buffer.bufferJob(result);
            }
        }
        return result;
    }

   
}

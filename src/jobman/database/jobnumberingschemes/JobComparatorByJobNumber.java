/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jobman.database.jobnumberingschemes;

import java.util.Comparator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import JobTypes.MainJob;

/**
 *
 * @author Robert Worrall
 */
public class JobComparatorByJobNumber implements Comparator {

    public int compare(MainJob o1, MainJob o2) {
        double jobNumber1 = formatJobNumber(o1.getJobNumber());
        double jobNumber2 = formatJobNumber(o2.getJobNumber());
        if (jobNumber1 < jobNumber2) {
            return 1;
        }
        if (jobNumber1 > jobNumber2) {
            return -1;
        } else {
            return 0;
        }
    }

    @Override
    public int compare(Object o1, Object o2) {        
        MainJob job1 = (MainJob) o1;
        MainJob job2 = (MainJob) o2;
       double jobNumber1 = formatJobNumber(job1.getJobNumber());
       double jobNumber2 = formatJobNumber(job2.getJobNumber());

        if (jobNumber1 < jobNumber2) {
            return 1;
        }
        if (jobNumber1 > jobNumber2) {
            return -1;
        } else {
            return 0;
        }
    }

    private double formatJobNumber(String jobNumber) {
        try {
            Matcher matcher = Pattern.compile("-").matcher(jobNumber);
            String replaceFirst = matcher.replaceFirst(".");
            matcher = Pattern.compile("-").matcher(replaceFirst);
            matcher.replaceAll("");
            return Double.parseDouble(matcher.replaceAll(""));
        } catch (NumberFormatException e) {
            return 0;
        }
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jobman.database.jobnumberingschemes;


public class SimpleJobNumberingScheme implements JobNumberingSchemeInterface {

    @Override
    public String issueJobMainNumber(int totalNumberOfJobs) {
     if(totalNumberOfJobs ==0)
        {
            return "1";
        }
    else{
        
    Integer returnValue = ++totalNumberOfJobs;
    //System.out.println("Generated new JobNumber" + lastJobNumber);
    return returnValue.toString();
    }
    }

    @Override
    public String issueSubJobNumber(String jobNumberOfParentJob, int currentNumberOfSubJobs) {
             
        if (currentNumberOfSubJobs == 0) {
            return jobNumberOfParentJob + "-1";
            
        } else {
           int newJobNumber =  currentNumberOfSubJobs+1;
            return jobNumberOfParentJob + "-" + newJobNumber;
            
        }
    }

    @Override
    public String getComparator() {
        return "jobman.database.jobnumberingschemes.JobComparatorByJobNumber";
    }

}

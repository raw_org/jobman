/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jobman.database.jobnumberingschemes;

/**
 *
 * @author worralr
 */
public interface JobNumberingSchemeInterface {
    public String issueJobMainNumber(int totalNumberOfJobs);
    public String issueSubJobNumber(String jobNumberOfParentJob, int currentNumberOfSubJobs);
    public String getComparator();
}

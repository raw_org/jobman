/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jobman.database;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.TimerTask;
import JobTypes.MainJob;

/**
 *
 * @author Robert Worrall
 */
public class JobBuffer extends TimerTask {

    private final List<MainJob> jobBuffer;
    // where keys are jobNumbers and values are times they have been accessed.
    // buffer of recently accessed MainJobs
    private final HashMap<String, Long> accessBufferList;
    private int maximumCapacity;

    public JobBuffer() {
        this.jobBuffer = new ArrayList();
        this.accessBufferList = new HashMap<>();
        this.maximumCapacity = 100;
    }

    public int getMaximumCapacity() {
        return maximumCapacity;
    }

    public void setMaximumCapacity(int maximumCapacity) {
        this.maximumCapacity = maximumCapacity;
    }

    public void bufferJob(MainJob job) {
        if (jobBuffered(job.getJobNumber())) {
            return;
        }
        if (jobBuffer.size() >= maximumCapacity) {
            removeOldestAccessedJob();
        }
        jobBuffer.add(job);
        accessBufferList.put(job.getJobNumber(), new Date().getTime());// time stamp the access of the job
    }

    public boolean jobBuffered(String jobNumber) {
        return accessBufferList.containsKey(jobNumber);
    }

    public MainJob getJob(String jobNumber) {
        if (jobBuffered(jobNumber)) {//see if the job is buffered
            for (MainJob job : jobBuffer) {
                if (job.getJobNumber().equals(jobNumber)) {// look at the jobNumber does it match what we are looking for.
                    return job;
                }
            }
        }
        return null;
    }

    public void removeJob(String jobNumber) {
        if (jobBuffered(jobNumber)) {
            jobBuffer.remove(getJob(jobNumber));
        }
        accessBufferList.remove(jobNumber);
    }

    void emptyBuffer() {
        jobBuffer.clear();
        accessBufferList.clear();
    }

    @Override
    public void run() {
        emptyBuffer();
    }

    private void removeOldestAccessedJob() {
        String jobNumber = findOldestAccessedJob();
        if (jobNumber.equals("")) {
            emptyBuffer();
        } else {
            removeJob(jobNumber);
        }

    }

    private String findOldestAccessedJob() {
        String jobNumber = "";
        long oldestDate = new Date().getTime();
        for (String key : accessBufferList.keySet()) {
            long jobAccessTime = accessBufferList.get(key);
            if (jobAccessTime < oldestDate) {
                jobNumber = key;
                oldestDate = jobAccessTime;
            }
        }
        return jobNumber;
    }

}

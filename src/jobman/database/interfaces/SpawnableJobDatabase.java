/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jobman.database.interfaces;

import JobTypes.BaseTypes.SpawnableJob;
import java.util.Set;

/**
 *
 * @author worralr
 */
public interface SpawnableJobDatabase {

    void deleteReplicableJob(SpawnableJob aJob);

    Set<SpawnableJob> getReplicatableJobs();

    void storeReplicatableJob(SpawnableJob aJob);
    
}

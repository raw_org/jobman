/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jobman.database.interfaces;

import JobTypes.BaseTypes.Job;
import JobTypes.MainJob;
import java.util.ArrayList;
import java.util.Set;
import jobman.database.DataBaseCreationOrOpeningException;

/**
 *
 * @author worralr
 */
public interface EmbeddedJobDatabase {

    void closeDatabase();

    void createDatabase(String filename, String numberingScheme) throws DataBaseCreationOrOpeningException;

    void deleteJob(Job aJob);

    Set<Job> findJob(final String searchString);

    MainJob findJobByJobNumber(final String jobNumber);

    Set<MainJob> getAllCurrentMainJobs();

    Set<Job> getAllFinshedMainJobs();

    ArrayList<String> getCatagoryList();

    String issueJobNumber();

    String issueSubJobNumber(String jobNumber, int size);

    void loadDatabase(String filename);

    void storeJob(Job aJob);
    
}

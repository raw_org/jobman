/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JobTypes.BaseTypes;

import JobTypes.MainJob;
import JobTypes.MainJobReplicator;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import jobman.JobException;

/**
 *
 * @author worralr
 */
public abstract class SpawnableJob{

private Date dateToSpawn;
    private ArrayList <SubJobReplicator> spawnableSubJobs;
    private boolean spawnWhenJobCompletes = false;
    private long replicationPeriod;// this is the length of time between spawning jobs
    private String existingJobNumber;
    private Job spawnedJob;
    private List <Job> spawnedSubjobs;
    private Job jobToBeReplicated;

    public Job getJobToBeReplicated() {
        return jobToBeReplicated;
    }

    public void setJobToBeReplicated(Job jobToBeReplicated) {
        this.jobToBeReplicated = jobToBeReplicated;
    }

    public boolean spawnOnceCompleted(){
    return spawnWhenJobCompletes;
    }
    
    
    
    
    public Job getLastSpawnedJob() {
        if (spawnedJob == null){
            try {
                spawn();
            } catch (JobException ex) {
                Logger.getLogger(MainJobReplicator.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return spawnedJob;
    }


    public List<Job> getSpawnedSubjobs() {
        return spawnedSubjobs;
    }

    

    public String getExistingJobNumber() {
        return existingJobNumber;
    }


    public void setExistingJobNumber(String existingJobName) {
        this.existingJobNumber = existingJobName;
    }
   

    public boolean isSpawnWhenJobCompletes() {
        return spawnWhenJobCompletes;
    }

    public void setSpawnWhenJobCompletes(boolean spawnWhenJobCompletes) {
        this.spawnWhenJobCompletes = spawnWhenJobCompletes;
    }
    
    
    /** The getter for the variable dateToSpawn
     * @return Date that job should spawn*/
    public Date getDateToSpawn(){
    return dateToSpawn;
    }
    
    /** The setter for the variable dateToSpaw,
     * @param aDate the Date job should spawn a new MainJob
     */

    public void setDateToSpawn(Date aDate){
    dateToSpawn = aDate;
    spawnedJob = null;
    }
    
    
    /** creates a MainJob that has all the attributes of MainJobReplicator
     * @return boolean if the spawn was successful
     * @throws jobman.JobException     
     */

    public boolean spawn() throws JobException{//First set up all the simple attributes of a MainJob
    spawnedJob = new MainJob(); // create the instance of mainJob
    configureSpawnedJob(spawnedJob);
    // Now set up the subJobs
    if(spawnableSubJobs == null || spawnableSubJobs.isEmpty()){                 
                    ArrayList<String> newList = new ArrayList();
                    spawnedJob.setLinkedJobs(newList);   
    }
    else{
    spawnedJob.setLinkedJobs(spawnSubJobs(spawnableSubJobs));
    }
    return true;
    }

    private void configureSpawnedJob(Job aSpawnedJob) {
        aSpawnedJob.setCategory(jobToBeReplicated.getCatagory());
        aSpawnedJob.setOtherReferenceNumber(jobToBeReplicated.getOtherReferenceNumber());
        aSpawnedJob.setName(jobToBeReplicated.getName());
        aSpawnedJob.setNotes(jobToBeReplicated.getNotes());     
        aSpawnedJob.setOtherDetails(jobToBeReplicated.getOtherDetails());
        aSpawnedJob.setPercentComplete(0);
        aSpawnedJob.setReminder(new Date());
    }


    public void setReplicationPeriod(long aReplicationPeriod) {
        replicationPeriod = aReplicationPeriod;
    }
    

    public long getReplicationPeriod() {
        return replicationPeriod;
    }
    
    /** This method creates an ArrayList of SubJobs that have all the attributes of an ArrayList of SubJobReplicator */
    private ArrayList <String> spawnSubJobs(ArrayList <SubJobReplicator> SubJobReplicator) {
    ArrayList <String> result = new ArrayList();
    SubJobReplicator currentReplicatableSubJob;
    MainJob currentSubJob;
    Iterator <SubJobReplicator> replicatableSubJobIterator = SubJobReplicator.iterator();
        while(replicatableSubJobIterator.hasNext()){
        currentReplicatableSubJob = replicatableSubJobIterator.next();
        currentSubJob = new MainJob();
        currentSubJob.setCategory(currentReplicatableSubJob.getCatagory());
        currentSubJob.setOtherReferenceNumber(currentReplicatableSubJob.getOtherReferenceNumber());
        currentSubJob.setName(currentReplicatableSubJob.getName());
        currentSubJob.setNotes(currentReplicatableSubJob.getNotes());
        currentSubJob.setJobNumber(currentReplicatableSubJob.getJobNumber());// This needs to be set in the JobManager
        currentSubJob.setOtherDetails(currentReplicatableSubJob.getOtherDetails());
        currentSubJob.setPercentComplete(0);
        if(currentReplicatableSubJob.getSpawnableSubJobs() != null){// see if there are more subjobs to replicate linked directly
                try {
                    // see if there are more subjobs to replicate linked directly
                    currentSubJob.setLinkedJobs(spawnSubJobs(currentReplicatableSubJob.getSpawnableSubJobs())); // now create ReplicateSubJobs those jobs by evoking the createReplicatableSubJobs method using the currentSubJobs linked SubJobs as the argument
                } catch (JobException ex) {
                    Logger.getLogger(MainJobReplicator.class.getName()).log(Level.SEVERE, null, ex);
                }
        }
        spawnedSubjobs.add(currentReplicatableSubJob);
        }
    return result;
    }
    
}
    
    /** This class is the data structure to store SubJobs*/
    class SubJobReplicator extends Job{
        private ArrayList <SubJobReplicator> spawnableSubJobs = new ArrayList();// holds the list of linked subJobReplicators
      
        //getter and setter for spawnableSubJobs
        /**The getter method for getting spawnable subjobs*/
        public ArrayList <SubJobReplicator> getSpawnableSubJobs(){
        return spawnableSubJobs;
        }
        
        /**The getter method for getting spawnable subjobs*/
        public void setSpawnableSubJobs(ArrayList <SubJobReplicator> aSpawnableJobList){
        spawnableSubJobs = aSpawnableJobList;
        }
        
    }
    


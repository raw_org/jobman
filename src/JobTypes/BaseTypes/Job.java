/*
 * Job.java
 *
 * Created on 27 September 2006, 17:37
 *
* Copyright (c) <2008>, <Robert Worrall>
 *  All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
    * Neither the name of the <ORGANIZATION> nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */

package JobTypes.BaseTypes;

import java.util.ArrayList;
import java.util.Date;
import jobman.JobException;

/**
 *
 * @author worralr
 */
public abstract class Job {


    private String name = "Create A Name";
    private long timeSpent = 0;
    private final Date creationDate;
    private Date startDate;
    private Date finishDate;
    private String notes = "";
    private int percentComplete = 0;
    private ArrayList<String> linkedJobs = new ArrayList();
    private String jobNumber;
    private String category = "none";
    private Date reminder;
    private int otherReferenceNumber;
    private String otherDetails = "";
    private int numberOfCreatedSubJobs = 0;

    /* Creates a new instance of Job */
    public Job() {
        creationDate = new Date();
    }

    public Job(String aJobNumber) {
        jobNumber = aJobNumber;
        creationDate = new Date();
    }

//Get and Set Methods for Job class variables
    /**
     * The set method for the name variable
     *
     * @param aName a String representing the name of the Job
     */
    public void setName(String aName) {
        name = aName;
    }

    /**
     * The get method for the name variable returns a String
     *
     * @return a string representing the name of a Job
     */
    public String getName() {
        return name;
    }

    /**
     * @return long timeSpent of this instance but not of all the other MainJobs
     * it is associated with
     */
    public long getTimeSpent() {
        return timeSpent;
    }

    /**
     * @param timeToIncrementBy increments the timespent on a job by the
     * specified amount.
     */
    public void incrementTimeSpent(long timeToIncrementBy) {
        if (timeToIncrementBy != 0) {
            timeSpent = timeSpent + timeToIncrementBy;
        }
    }

    /**
     *
     * @param timeSpent the length of time spent on a job
     */
    public void setTimeSpentTo(long timeSpent) {
        this.timeSpent = timeSpent;
    }

    /**
     * The get method for startDate variable
     *
     * @return the date the job was started
     */
    public Date getstartDate() {
        return startDate;

    }

    /**
     * The set method for the startDate variable
     *
     * @param jobstartDate the date the job was started
     */
    public void setstartDate(Date jobstartDate) {
        startDate = jobstartDate;
    }

    /**
     * The get method for the creationDate variable returns a Date object
     *
     * @return the date the job was created
     */
    public Date getCreationDate() {
        return creationDate;
    }

    /**
     * returns a Date object
     *
     * @return Date representing the finish date the job
     */
    public Date getFinshDate() {
        return finishDate;
    }

    /**
     * @param jobFinishDate Date the job was finished
     */
    public void setFinishDate(Date jobFinishDate) {
        finishDate = jobFinishDate;
    }

    /**
     * The get method for the notes variable returns a JTextArea object
     *
     * @return String representing jobs attached to the job.
     */
    public String getNotes() {
        return notes;
    }

    /**
     * The set method for notes variable
     *
     * @param notes A String representing the notes attached to a Job
     */
    public void setNotes(String notes) {
        this.notes = notes;
    }

    /**
     * The get method for percentageComplete variable
     *
     * @return an int representing the percentage the job is complete.
     */
    public int getPercentComplete() {
        return percentComplete;
    }

    /**
     * The set method for percentageComplete variable this will return false and
     * will not go above a given percentage if a subjob is not complet
     *
     * @param percentage the percentage a job is complete
     *
     */
    public void setPercentComplete(int percentage) {
        //boolean result = false;
        if ((percentage > 100) || (percentage < 0)) {
            if (percentage > 100) {
                percentComplete = 100;
            } else {
                percentComplete = 0;
            }

        } else {
            percentComplete = percentage;

        }

    }

    /**
     * The get method for the linkedJobs variable
     *
     * @return returns a list of strings representing job numbers linked to this
     * job.
     */
    public ArrayList<String> getLinkedJobs() {
        return linkedJobs;
    }

    /**
     * The set method for the linkedJobs variable if any of the subjobs
     * jobnumber does not begin with the this.jobnumber then linkedJobList is
     * not updated.
     *
     * @param linkedJobList a list of Strings that represent linked jobs.
     * @throws jobman.JobException throws an exception if a subjob jobnumber is
     * invalid
     */
    public void setLinkedJobs(ArrayList<String> linkedJobList) throws JobException {
        if (validateLinkedJobs(linkedJobList)) {
            linkedJobs = linkedJobList;
        } else {
            throw new JobException("Unable to attach subjobs. A Subjob missmatch!");
        }
    }

    /**
     * This method is to ensure that subjobs are added correctly and are true
     * sujobs i.e the subjob begins with a jobNumber
     *
     * @param jobNumber String representing a jobNumber to be linked to another
     * job.
     * @throws jobman.JobException when an invalid jobnumber is supplied.
     */
    public void addSubJob(String jobNumber) throws JobException {
        if (validateJob(jobNumber)) {
            linkedJobs.add(jobNumber);
        } else {
            throw new JobException("Error trying to add subjob. not a true subjob type");
        }
    }

    /**
     * The get method the jobNumber variable returns an int
     *
     * @return returns the jobnumber of the current task
     */
    public String getJobNumber() {
        return jobNumber;
    }

    /**
     * The set method for the jobNumber variable
     *
     * @param aJobNumber a String representing the jobNumber of the Job
     */
    public void setJobNumber(String aJobNumber) {
        jobNumber = aJobNumber;
    }

    /**
     * The get method for the catagory variable
     *
     * @return String describing the jobs catagory
     */
    public String getCatagory() {
        return category;
    }

    /**
     * @param aCategory describing the jobs catagory
     */
    public void setCategory(String aCategory) {
        category = aCategory;
    }

    /**
     *
     * @return Date job is due
     */
    public Date getReminder() {
        return reminder;
    }

    /**
     * The set method for the reminder variable
     *
     * @param aReminder Date job is due
     */
    public void setReminder(Date aReminder) {
        reminder = aReminder;
    }

    /**
     * The get method for the patientNumber variable returns an int
     *
     * @return int that represents another referrence number e.g an order number
     */
    public int getOtherReferenceNumber() {
        return otherReferenceNumber;
    }

    /**
     * The set method for the patientNumber variable
     *
     * @param aCustomerNumber int that represents another referrence number e.g
     * an order number
     */
    public void setOtherReferenceNumber(int aCustomerNumber) {
        otherReferenceNumber = aCustomerNumber;
    }

    /**
     * The set method for the otherDetails variable
     *
     * @param details String other details associated with a job
     */
    public void setOtherDetails(String details) {
        otherDetails = details;
    }

    /**
     * The get method for the otherDetails variable
     *
     * @return String other details associated with a job
     */
    public String getOtherDetails() {
        return otherDetails;
    }

    /**
     * The get method for the numberOfSubJobs variable
     *
     */
    private int getNumberOfCreatedSubJobs() {
        return numberOfCreatedSubJobs;
    }

    /**
     * The get method for the numberOfSubJobs variable
     */
    private void setNumberOfCreatedSubJobs(int aNumber) {
        numberOfCreatedSubJobs = aNumber;
    }

//    /**
//     * The method that issues a new Job Name for a SubJob of this Job
//     *
//     * @return String representing a valid Subjob number 
//     *
//     */
//    public String issueSubJobNumber() {
//        String result;
//        if (getNumberOfCreatedSubJobs() == 0) {
//            result = getJobNumber() + "-1";
//            setNumberOfCreatedSubJobs(1);
//            return result;
//        } else {
//            setNumberOfCreatedSubJobs(getNumberOfCreatedSubJobs() + 1);
//            result = getJobNumber() + "-" + getNumberOfCreatedSubJobs();
//            return result;
//        }
//    }

    /**
     * This method checks to see if the list of jobnames are truly subjobs of
     * the job. to be true subjobs they must begin with this jobs jobnumber.
     */
    private boolean validateLinkedJobs(ArrayList<String> linkedJobList) {
        boolean result = true;
        for (String subjobnumber : linkedJobList) {
            if (validateJob(subjobnumber)) {
            } else {
                result = false;
            }
        }

        return result;
    }

    /**
     * method to test if the subjob starts with this jobs jobnumber
     */
    private boolean validateJob(String aJobNumber) {
        return aJobNumber.startsWith(jobNumber);
    }
}



/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jobFactory;

import JobTypes.BaseTypes.Job;

/**
 *
 * @author worralr
 */
public interface JobFactory {
    
    public void setJobType(String type);
    public Job createJob() throws JobTypeNotValidException;
    
}

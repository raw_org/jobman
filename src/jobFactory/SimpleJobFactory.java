/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jobFactory;

import JobTypes.BaseTypes.Job;
import JobTypes.BaseTypes.SpawnableJob;
import java.util.Date;

/**
 *
 * @author worralr
 */
public class SimpleJobFactory implements JobFactory, SpawnableJobFactory {

    private String jobType;
    private String spawnableJobType;

    @Override
    public void setJobType(String type) {
        this.jobType = type;
    }

    @Override
    public Job createJob() throws JobTypeNotValidException{
        try{
        return (Job) Class.forName(jobType).newInstance();
        }
        catch (ClassNotFoundException | IllegalAccessException | InstantiationException | ClassCastException e){
            throw new JobTypeNotValidException(jobType + " is not a valid Job Type " + e);
        }
    }

    @Override
    public void setSpawnableType(String spawnableJobType) {
        this.spawnableJobType = spawnableJobType;
    }

    @Override
    public SpawnableJob createSpawnableJob(Job jobtobespawned, Date dateToSpawn, boolean spawnWhenJobCompletes, long replicationPeriod) throws JobTypeNotValidException{
        try{
            SpawnableJob spawnable = (SpawnableJob) Class.forName(spawnableJobType).newInstance();
            spawnable.setJobToBeReplicated(jobtobespawned);
            spawnable.setDateToSpawn(dateToSpawn);
            spawnable.setSpawnWhenJobCompletes(spawnWhenJobCompletes);
            spawnable.setReplicationPeriod(replicationPeriod);
        return spawnable;
        }
        catch (ClassNotFoundException | IllegalAccessException | InstantiationException | ClassCastException e){
            throw new JobTypeNotValidException(jobType + " is not a valid Job Type " + e);
        }
    }

    @Override
    public SpawnableJob createSpawnableJob(Job jobtobespawned) throws JobTypeNotValidException {
        return this.createSpawnableJob(jobtobespawned, null, false, 0);
    }

}

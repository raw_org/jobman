/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jobFactory;

/**
 *
 * @author worralr
 */
class JobTypeNotValidException extends Exception {

    public JobTypeNotValidException(String error) {
        super(error);
    }
 
   
}

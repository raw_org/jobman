/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jobFactory;

import JobTypes.BaseTypes.Job;
import JobTypes.BaseTypes.SpawnableJob;
import java.util.Date;


/**
 *
 * @author worralr
 */
public interface SpawnableJobFactory {
    
    public void setSpawnableType(String spawnableJobType);
    
    public SpawnableJob createSpawnableJob(Job jobtobespawned, Date dateToSpawn,boolean spawnWhenJobCompletes, long replicationPeriod)  throws JobTypeNotValidException;
    
    public SpawnableJob createSpawnableJob(Job jobtobespawned)  throws JobTypeNotValidException;
}
